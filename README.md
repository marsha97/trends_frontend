## Frontend Program for Trends
This program is a frontend part of Trends. Made with Angular 6.

## What is Trends?
Trends is a web that can help you find popular products in Twitter by comparing your keyword to the found tweets, and you can also check the sentiment value of certain products. This app provides frontend that present the result from API.

## Limitation
The Twitter search is limited up to 5 query (about 500 tweets) to save the Twitter API limitation. And the compare product feature is limited to 3 products since the Twitter API limimation.