import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TwtSearchComponent } from './twt-search/twt-search.component';
import { SentimentDetailComponent } from './sentiment-detail/sentiment-detail.component';
import { TweetListComponent } from './tweet-list/tweet-list.component';
import { TestSentimentComponent } from './test-sentiment/test-sentiment.component';
import { ProductSearchComponent } from './product-search/product-search.component';
import { CompareProductsComponent } from './compare-products/compare-products.component';
import { PreprocessingSentiComponent } from './preprocessing-senti/preprocessing-senti.component';
import { PreprocessingProductComponent } from './preprocessing-product/preprocessing-product.component';

const routes: Routes = [
  {path: 'search', component: TwtSearchComponent},
  {path: 'detail', component: SentimentDetailComponent},
  {path: 'products', component: TweetListComponent},
  {path: 'test', component: TestSentimentComponent},
  {path: 'lists', component: ProductSearchComponent},
  {path: 'compare', component: CompareProductsComponent},
  {path: 'presenti/:id', component: PreprocessingSentiComponent},
  {path: 'preproduct/:id', component: PreprocessingProductComponent},
  {path: '', redirectTo: '/search', pathMatch: 'full'}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
