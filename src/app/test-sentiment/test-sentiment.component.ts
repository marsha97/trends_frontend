import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TestCommentService } from '../test-comment.service';
import { Sentiment } from '../sentiment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-test-sentiment',
  templateUrl: './test-sentiment.component.html',
  styleUrls: ['./test-sentiment.component.css']
})
export class TestSentimentComponent implements OnInit, OnDestroy {

  commentInput: string;
  result: Sentiment;
  commentForm: FormGroup;
  showInvalid: boolean = false;
  isLoading: boolean = false;
  hasError = false;
  errorMsg = "";
  goDisplay: boolean = false;

  private getCommentSubs: Subscription;


  constructor(private fb: FormBuilder,
              private commentSvc: TestCommentService) { }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy(){
    if(this.getCommentSubs){
      this.getCommentSubs.unsubscribe();
    }
  }

  createForm(){
    this.commentForm = this.fb.group({
      comment: ['', Validators.required],
    });
  }

  processQuery(){
    this.isLoading = true;
    this.goDisplay = true;
    if(this.commentForm.status == "INVALID"){
      this.showInvalid = true;
    }
    else{
      this.showInvalid = false;
      this.getCommentSubs = this.commentSvc.getComment(this.commentForm.get('comment').value).subscribe(
        result => {this.result = result; this.isLoading = false}, 
        error => this.errorOperation(error),
      )
    }
  }

  errorOperation(msg: string){
    this.hasError = true;
    this.isLoading = false;
    this.errorMsg = msg;
  }

}
