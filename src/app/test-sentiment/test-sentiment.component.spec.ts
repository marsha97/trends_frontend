import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSentimentComponent } from './test-sentiment.component';

describe('TestSentimentComponent', () => {
  let component: TestSentimentComponent;
  let fixture: ComponentFixture<TestSentimentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestSentimentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSentimentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
