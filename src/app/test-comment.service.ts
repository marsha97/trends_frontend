import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Sentiment } from './sentiment';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

const httpOptions= {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8'
  }),
  params: new HttpParams(),
  reportProgress: true,
}

@Injectable({
  providedIn: 'root'
})
export class TestCommentService {

  private APIurl: string = 'http://127.0.0.1:8000/tweets/comment/';

  constructor(private http:HttpClient) { }

  getComment(comment){
    httpOptions.params = comment ? 
      httpOptions.params.set('comment', comment) :
      httpOptions.params.set('comment', '')
    return this.http.get<Sentiment>(this.APIurl, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
