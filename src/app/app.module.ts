import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MatPaginatorModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProductSearchComponent } from './product-search/product-search.component';
import { SentimentSearchComponent } from './sentiment-search/sentiment-search.component';
import { TwtSearchComponent } from './twt-search/twt-search.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { SentimentDetailComponent } from './sentiment-detail/sentiment-detail.component';
import { TweetListComponent } from './tweet-list/tweet-list.component';
import { TestSentimentComponent } from './test-sentiment/test-sentiment.component';
import { CompareProductsComponent } from './compare-products/compare-products.component';
import { PreprocessingSentiComponent } from './preprocessing-senti/preprocessing-senti.component';
import { PreprocessingProductComponent } from './preprocessing-product/preprocessing-product.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProductSearchComponent,
    SentimentSearchComponent,
    TwtSearchComponent,
    SearchResultComponent,
    SentimentDetailComponent,
    TweetListComponent,
    TestSentimentComponent,
    CompareProductsComponent,
    PreprocessingSentiComponent,
    PreprocessingProductComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ChartsModule,
    MatPaginatorModule,
    NoopAnimationsModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
