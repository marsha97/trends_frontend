export interface Products{
    raw_tweets: string[],
    products: Product[],
    queries_done: number,
    tweets_count: number,
    data: Data[],
}

export interface Data{
    id: number
    raw_tweet: string,
    clean: string,
    token: string[]
}

export interface Product{
    name: string,
    count: number
}