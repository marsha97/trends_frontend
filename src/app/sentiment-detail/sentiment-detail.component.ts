import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SentimentService } from '../sentiment.service';
import { PageEvent} from '@angular/material';
import { Data, Sentiment } from '../sentiment';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sentiment-detail',
  templateUrl: './sentiment-detail.component.html',
  styleUrls: ['./sentiment-detail.component.css']
})
export class SentimentDetailComponent implements OnInit, OnDestroy {

  filter: string = ''
  displayedColumns = ['text']
  dataSource: MatTableDataSource<Data>

  pageEvent: PageEvent

  SentimentData: Object

  tabelContent: Sentiment['data']

  private currSentiSubs: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator

  constructor(
    private sentimentSvc: SentimentService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.currSentiSubs = this.sentimentSvc.currSentiObj.subscribe(data => {this.SentimentData = data; this.afterLoad()}, error => {})
  }  

  ngOnDestroy(): void {
    if(this.currSentiSubs){
      this.currSentiSubs.unsubscribe();
    }
  }
  afterLoad(){
    this.tabelContent = this.SentimentData['data']
    this.dataSource = new MatTableDataSource<Data>(this.tabelContent)
    this.dataSource.paginator = this.paginator
    this.dataSource.filterPredicate = this.customFilter
  }

  filterData(label: string){
    this.filter = label
    this.dataSource.filter = label
    // console.log(label);
    // console.log(this.dataSource);  
  }

  customFilter(data: Data, label: string):boolean{
    return data['label'] == label
  }

  navigateToPresenti(id:number){
    this.router.navigate(['presenti', id])
  }
  goBack(){
    this.location.back()
  }
}