import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, BehaviorSubject } from 'rxjs';
import { Products } from './products';

const httpOptions= {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8'
  }),
  params: new HttpParams(),
  reportProgress: true,
}

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private PRODUCT_DEFAULT: Products = {data: [], raw_tweets: [], products: [], queries_done: 0, tweets_count: 0}

  APIurl: string = 'http://127.0.0.1:8000/tweets/products/'

  private productsSrc = new BehaviorSubject(this.PRODUCT_DEFAULT)
  currentProducts = this.productsSrc.asObservable()

  private checkedSrc = new BehaviorSubject([])
  currentChecked = this.checkedSrc.asObservable()

  constructor(private http: HttpClient) { }

  getProducts(keyword){
    httpOptions.params = keyword ? 
      httpOptions.params.set('keyword', keyword) :
      httpOptions.params.set('keyword', '')
    return this.http.get<Products>(this.APIurl, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  setProducts(obj: Products){
    this.productsSrc.next(obj)
  }

  resetProducts(){
    this.productsSrc.next(this.PRODUCT_DEFAULT)
  }
  
  setChecked(checked: string[]){
    this.checkedSrc.next(checked);
  }

  getChecked(){
    return this.checkedSrc;
  }

  resetChecked(){
    this.checkedSrc.next([])
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
