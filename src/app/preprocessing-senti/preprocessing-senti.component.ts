import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SentimentService } from '../sentiment.service';
import { Sentiment, Data } from '../sentiment';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-preprocessing-senti',
  templateUrl: './preprocessing-senti.component.html',
  styleUrls: ['./preprocessing-senti.component.css']
})
export class PreprocessingSentiComponent implements OnInit, OnDestroy {
  currSentiSubs: Subscription;
  SentimentData: Sentiment;
  prepData: Data;
  id: number

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sentimentSvc: SentimentService
  ) { }

  ngOnInit() {
    this.currSentiSubs = this.sentimentSvc.currSentiObj.subscribe(data => {this.SentimentData = data; this.afterLoad()}, error => {})
  }

  ngOnDestroy(): void {
    if(this.currSentiSubs){
      this.currSentiSubs.unsubscribe();
    }
  }
  afterLoad() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.prepData = this.SentimentData.data[this.id]
  }
}
