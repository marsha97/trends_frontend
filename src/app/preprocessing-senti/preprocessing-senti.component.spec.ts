import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreprocessingSentiComponent } from './preprocessing-senti.component';

describe('PreprocessingSentiComponent', () => {
  let component: PreprocessingSentiComponent;
  let fixture: ComponentFixture<PreprocessingSentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreprocessingSentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreprocessingSentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
