import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private isProdSrc = new BehaviorSubject(true)
  currentisProd = this.isProdSrc.asObservable()

  private keywordSrc = new BehaviorSubject("")
  currentKeyword = this.keywordSrc.asObservable();

  private prodLoadSrc = new BehaviorSubject(false)
  currentProdLoad = this.prodLoadSrc.asObservable()

  constructor() { }

  setIsProd(obj: boolean){
    this.isProdSrc.next(obj)
  }

  setKeywordObj(obj: string){
    this.keywordSrc.next(obj)
  }

  setProgLoad(obj: boolean){
    this.prodLoadSrc.next(obj)
  }
}
