import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentimentSearchComponent } from './sentiment-search.component';

describe('SentimentSearchComponent', () => {
  let component: SentimentSearchComponent;
  let fixture: ComponentFixture<SentimentSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentimentSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentimentSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
