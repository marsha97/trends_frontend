import { Component, OnChanges, Input, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { SentimentService } from '../sentiment.service';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Sentiment } from '../sentiment';
import { GeneralService } from '../general.service';

@Component({
  selector: 'app-sentiment-search',
  templateUrl: './sentiment-search.component.html',
  styleUrls: ['./sentiment-search.component.css']
})
export class SentimentSearchComponent implements OnInit, OnDestroy {

  public doughnutChartLabels: string[]
  public doughnutChartData: number[]
  public doughnutChartType: string
  public doughnutColors: Array<any>

  data: Object
  positive: number
  negative: number
  errorMsg: string
  hasError = false
  keywordInput: string
  SentimentData: Sentiment

  isLoading: boolean = true

  private currSentiSubs: Subscription;
  private currKeywordSubs: Subscription;
  private getSentimentSubs: Subscription;

  constructor(private route: ActivatedRoute, 
    private sentimentSvc: SentimentService,
    private generalSvc: GeneralService) {
    // this.route.params.subscribe( params => this.SentimentData = params.keyword)
  }

  ngOnInit() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.currSentiSubs = this.sentimentSvc.currSentiObj.subscribe(sentiment => {this.SentimentData = sentiment; 
      this.loadCache()})
  }

  loadCache(){
    this.currKeywordSubs = this.generalSvc.currentKeyword.subscribe(key => this.keywordInput = key)
    
    if(this.SentimentData['data'].length == 0){
      this.isLoading = true
      this.getSentimentSubs = this.sentimentSvc.getSentiment("\"" + this.keywordInput + "\"").subscribe(
        sentiment => this.SentimentData = sentiment, error => this.errorOperation(error)
        , () => this.nextOperations()
      )
    }
    else{
      this.isLoading = false
      this.makeChart()
    }
  }
  
  errorOperation(msg: string){
    this.hasError = true
    this.isLoading = false
    this.errorMsg = msg
  }

  nextOperations() {
    // console.log(this.errorMsg)
    this.hasError = false
    console.log(this.hasError);
    
    this.sentimentSvc.setSentiObj(this.SentimentData)
    this.isLoading = false
    this.makeChart()
  }

  makeChart(){
    this.data = Object.keys(this.SentimentData).map(key => this.SentimentData[key])
    this.positive = this.data[1]
    this.negative = this.data[2]

    this.doughnutChartLabels = ['Positive', 'Negative'];
    this.doughnutChartData = [this.positive, this.negative];
    this.doughnutChartType = 'doughnut';
    this.doughnutColors = [
      {backgroundColor: ['#21c5dd','#d73c3e']},
    ]
  }

  setLoading(state) {
    this.isLoading = state
  }

  ngOnDestroy(): void {
    if(this.currKeywordSubs){
      this.currKeywordSubs.unsubscribe();
    }
    if(this.currSentiSubs){
      this.currSentiSubs.unsubscribe();
    }
    // this.getSentimentSubs.unsubscribe()
  }
}
