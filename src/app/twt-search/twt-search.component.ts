import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralService } from '../general.service';
import { SentimentService } from '../sentiment.service';
import { ProductsService } from '../products.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-twt-search',
  templateUrl: './twt-search.component.html',
  styleUrls: ['./twt-search.component.css']
})
export class TwtSearchComponent implements OnInit, OnDestroy {

  keywordForm: FormGroup
  
  buttonClass: string = "btn btn-custom "
  selectedProduct: string = this.buttonClass + "active"
  selectedSentiment: string = this.buttonClass

  keywordInput: string

  selected: string = "product"

  isProduct: boolean = true
  isProduct_temp: boolean = true
  showValid: boolean = false

  private currKeywordSubs: Subscription;
  private currIsProdSubs: Subscription;

  selectButton(selectedBtn: string) {
    if (selectedBtn == "product") {
      this.selectedProduct = this.buttonClass + "active"
      this.selectedSentiment = this.buttonClass
      this.isProduct_temp = true
    }
    else {
      this.selectedProduct = this.buttonClass
      this.selectedSentiment = this.buttonClass + "active"
      this.isProduct_temp = false
    }
    this.selected = selectedBtn
  }

  constructor(
    private fb: FormBuilder, 
    private generalSvc: GeneralService, 
    private sentimentSv: SentimentService,
    private productSvc: ProductsService,
    private router: Router) { 
    this.currKeywordSubs = this.generalSvc.currentKeyword.subscribe(data => {this.keywordInput = data; this.createForm()}, error => {})
    this.currIsProdSubs = this.generalSvc.currentisProd.subscribe(data => {this.isProduct = data; this.selectButton(this.isProduct ? "product": "sentiment")})
  }

  ngOnDestroy(){
    if(this.currIsProdSubs){
      this.currIsProdSubs.unsubscribe();
    }
    if(this.currKeywordSubs){
      this.currIsProdSubs.unsubscribe();
    }
  }

  createForm(){
    this.keywordForm = this.fb.group({
      keyword: ['', Validators.required],
    })
  }

  isValid(){
    if(this.keywordForm.status == "INVALID"){
      this.showValid = true      
    }
    else{
      this.showValid = false  
      this.isProduct = this.isProduct_temp
    }

    if(this.keywordForm.get('keyword').value != this.keywordInput && !this.showValid){
      this.keywordInput = this.keywordForm.get('keyword').value
      this.generalSvc.setKeywordObj(this.keywordInput)
      this.generalSvc.setIsProd(this.isProduct)
      this.sentimentSv.resetSentiObj()
      this.productSvc.resetProducts()
    }

    if(this.isProduct && !this.showValid){
      this.router.navigate(['lists'])
    }
  }

  ngOnInit() {
  }

}
