import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwtSearchComponent } from './twt-search.component';

describe('TwtSearchComponent', () => {
  let component: TwtSearchComponent;
  let fixture: ComponentFixture<TwtSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwtSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwtSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
