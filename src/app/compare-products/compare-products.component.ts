import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductsService } from '../products.service';
import { takeUntil } from 'rxjs/operators';
import { Sentiment } from '../sentiment';
import { Subject, Observable, Subscription } from 'rxjs';
import { SentimentService } from '../sentiment.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-compare-products',
  templateUrl: './compare-products.component.html',
  styleUrls: ['./compare-products.component.css']
})
export class CompareProductsComponent implements OnDestroy, OnInit {
  public barChartOptions: any = {
    scaleShowVerticalLines: true,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barColors:any[] = [];
  public barChartData: object[] = [];

  products: string[];
  selecteds: Sentiment[] = [];
  isLoading: boolean = false;

  private checkedSubs: Subscription;
  private sentimentSubs: Subscription;
  constructor(
    private productSvc: ProductsService,
    private sentimentSvc: SentimentService,
    private location: Location,
    private router: Router,
  ) { }

  ngOnInit() {
    this.barChartData[0] = {data: [], label: "Positive", backgroundColor: '#21c5dd'};
    this.barChartData[1] = {data: [], label: "Negative", backgroundColor: '#d73c3e'};
    this.checkedSubs = this.productSvc.currentChecked.subscribe(p => {
      this.products = p;
      if (this.products.length > 0) {
        this.afterLoad();
      }
    })
  }

  ngOnDestroy(): void {
    if (this.checkedSubs) {
      this.checkedSubs.unsubscribe();
    }
    if (this.sentimentSubs) {
      this.sentimentSubs.unsubscribe();
    }
  }

  afterLoad() {
    this.selecteds = this.sentimentSvc.getSentis();
    console.log(this.selecteds.length);
    if (this.selecteds.length == 0) {
      this.isLoading = true;
      this.getSentimentData();
    }
    else{
      this.makeChart();
    }
  }

  private counter: number = 0;
  getSentimentData() {
    this.sentimentSvc.getSentiment(this.products[this.counter]).subscribe(s => {
      this.selecteds.push(s);
      this.counter++;
      if (this.counter < this.products.length) {
        this.getSentimentData();
      }
      else {
        this.sentimentSvc.setSentis(this.selecteds);
        this.isLoading = false;
        this.makeChart();
      }
    })
  }

  private makeChart() {
    this.selecteds.forEach(sentiment => {
      this.barChartLabels.push(sentiment['keyword']);
      this.barChartData[0]['data'].push(sentiment['positive']);
      this.barChartData[1]['data'].push(sentiment['negative']);
    });    
  }

  getDetail(index: number){
    this.sentimentSvc.setSentiObj(this.selecteds[index]);
    this.router.navigate(['detail']);
  }

  goBack(){
    this.location.back();
  }
}
