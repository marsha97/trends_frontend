import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ProductsService } from '../products.service';
import { MatTableDataSource, PageEvent, MatPaginator } from '@angular/material';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { Data } from '../products';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tweet-list',
  templateUrl: './tweet-list.component.html',
  styleUrls: ['./tweet-list.component.css']
})
export class TweetListComponent implements OnInit, OnDestroy {

  filter: string = ''
  displayedColumns = ['text']
  dataSource: MatTableDataSource<Data>

  pageEvent: PageEvent

  @ViewChild(MatPaginator) paginator: MatPaginator
  
  private currProductSubs: Subscription;

  tweets: string[]
  datas: Data[]

  constructor(
    private productSvc: ProductsService,
    private location: Location,
    private router: Router,
  ) { }

  ngOnInit() {
    this.currProductSubs = this.productSvc.currentProducts.subscribe(data => {this.datas = data['data']; this.afterLoad()})
  }

  ngOnDestroy(){
    if(this.currProductSubs){
      this.currProductSubs.unsubscribe();
    }
  }
  
  afterLoad() {
    this.dataSource = new MatTableDataSource<Data>(this.datas)
    this.dataSource.paginator = this.paginator
  }

  applyFilter(filterValue: string) {
    console.log(filterValue);
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  navigateToPreProduct(id: number){
    this.router.navigate(['preproduct', id])
  }

  goBack(){
    this.location.back()
  }
}
