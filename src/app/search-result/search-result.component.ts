import { Component, Input, OnChanges, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { GeneralService } from '../general.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit, OnDestroy {
  keywordInput: string
  isProduct: boolean
  
  private currentKeywordSubs: Subscription;
  private currentIsProdSubs: Subscription;

  constructor(private generalSvc:GeneralService) { }
  
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.currentKeywordSubs = this.generalSvc.currentKeyword.subscribe(data => {this.keywordInput = data;;
    }, error => {})
    this.currentIsProdSubs = this.generalSvc.currentisProd.subscribe(data => this.isProduct = data)    
  }

  ngOnDestroy(): void {
    if(this.currentIsProdSubs){
      this.currentIsProdSubs.unsubscribe();
    }
    if(this.currentKeywordSubs){
      this.currentKeywordSubs.unsubscribe();
    }
  }
}
