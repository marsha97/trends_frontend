import { TestBed, inject } from '@angular/core/testing';

import { TestCommentService } from './test-comment.service';

describe('TestCommentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TestCommentService]
    });
  });

  it('should be created', inject([TestCommentService], (service: TestCommentService) => {
    expect(service).toBeTruthy();
  }));
});
