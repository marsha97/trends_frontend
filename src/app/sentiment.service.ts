import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {Sentiment} from './sentiment'

const httpOptions= {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8'
  }),
  params: new HttpParams(),
  reportProgress: true,
}

@Injectable({
  providedIn: 'root'
})
export class SentimentService {
  
  private default_obj: Sentiment = {data: [], positive: 0, negative: 0, tweets_count: 0, queries_done: 0, keyword: ''}

  private sentiObjSrc = new BehaviorSubject(this.default_obj)
  currSentiObj = this.sentiObjSrc.asObservable();

  private sentis_default: Sentiment[] = [];
  private sentisSrc = new BehaviorSubject(this.sentis_default);
  currSentis = this.sentisSrc.asObservable();

  private APIurl: string = 'http://127.0.0.1:8000/tweets/sentiment/'

  constructor(private http:HttpClient) {}

  getSentiment(keyword){
    httpOptions.params = keyword ? 
      httpOptions.params.set('keyword', keyword) :
      httpOptions.params.set('keyword', '')
    return this.http.get<Sentiment>(this.APIurl, httpOptions).pipe(
      catchError(this.handleError)
    );
  }
  
  setSentiObj(obj: Sentiment){
      this.sentiObjSrc.next(obj)
  }

  resetSentiObj(){
    this.sentiObjSrc.next(this.default_obj)
  }

  setSentis(obj: Sentiment[]){
    this.sentisSrc.next(obj);
  }

  resetSentis(){
    this.sentisSrc.next([]);    
  }

  getSentis(){
    return this.sentisSrc.getValue();
  }
  
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
