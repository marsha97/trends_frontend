export interface Sentiment {
    data: Data[]
    positive: number
    negative: number
    tweets_count: number
    queries_done: number
    keyword: string
}

export interface Data {
    id: number
    raw_text: string
    text: string[]
    label: string
    clean: string
    token: string[]
    stem: string[]
}

export interface Detail{
    word: string
    label_w: string
}