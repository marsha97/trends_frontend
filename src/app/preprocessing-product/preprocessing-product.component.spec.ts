import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreprocessingProductComponent } from './preprocessing-product.component';

describe('PreprocessingProductComponent', () => {
  let component: PreprocessingProductComponent;
  let fixture: ComponentFixture<PreprocessingProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreprocessingProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreprocessingProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
