import { Component, OnInit, OnDestroy } from '@angular/core';
import { Products, Data } from '../products';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-preprocessing-product',
  templateUrl: './preprocessing-product.component.html',
  styleUrls: ['./preprocessing-product.component.css']
})
export class PreprocessingProductComponent implements OnInit, OnDestroy {
  currProSubs: Subscription;
  ProductDatas: Data[] ;
  prepData: Data;
  id: number

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productSvc: ProductsService
  ) { }

  ngOnInit() {
    this.currProSubs = this.productSvc.currentProducts.subscribe(data => {this.ProductDatas = data['data']; this.afterLoad()}, error => {})
  }

  ngOnDestroy(): void {
    if(this.currProSubs){
      this.currProSubs.unsubscribe();
    }
  }
  afterLoad() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.prepData = this.ProductDatas[this.id]
  }
}
