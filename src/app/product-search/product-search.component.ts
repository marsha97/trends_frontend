import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { GeneralService } from '../general.service';
import { ProductsService } from '../products.service';
import { Products, Product } from '../products';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataSource } from '@angular/cdk/table';
import { MatTableDataSource, PageEvent, MatPaginator } from '@angular/material';
import { SentimentService } from '../sentiment.service';
import { Sentiment } from '../sentiment';
import { Router } from '@angular/router';
import { Subscriber, Subscription } from 'rxjs';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css']
})
export class ProductSearchComponent implements OnInit, OnDestroy {
  displayedColumns = ['name', 'count']
  dataSource: MatTableDataSource<Product>
  filterForm: FormGroup
  
  pageEvent = PageEvent

  tableContent: Product[]

  @ViewChild(MatPaginator) paginator: MatPaginator
  keywordInput: string
  ProductsData: Products
  RawTweets: Products['raw_tweets']
  prod_isLoading: boolean = false
  products: string[]
  product_check:{[key: string]: boolean} = {}
  clicked_index: number
  isLoading: boolean
  hasError: boolean
  errorMsg: string

  SentimentData: Sentiment

  products_valid:boolean
  products_error: string
  checked: string[]

  private currentProductsSubs: Subscription;
  private currentKeywordSubs: Subscription;
  private getProductsSubs: Subscription;
  private getSentimentSubs: Subscription;
  checkedSubs: Subscription = new Subscription();
  checked_tmp: string[];

  constructor(
    private generalSvc: GeneralService,
    private productSvc: ProductsService,
    private sentimentSvc: SentimentService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.currentProductsSubs = this.productSvc.currentProducts.subscribe(products => { this.ProductsData = products; this.loadCache() })
  }

  ngOnDestroy(): void {
    if(this.currentProductsSubs){
      this.currentProductsSubs.unsubscribe();
    }
    if(this.currentKeywordSubs){
      this.currentKeywordSubs.unsubscribe();
    }
    if(this.checkedSubs){
      this.checkedSubs.unsubscribe();
    }
  }

  loadCache() {
    this.currentKeywordSubs = this.generalSvc.currentKeyword.subscribe(key => this.keywordInput = key)

    if (this.ProductsData['raw_tweets'].length == 0) {
      this.isLoading = true
      this.getProductsSubs = this.productSvc.getProducts(this.keywordInput).subscribe(
        products => this.ProductsData = products, error => this.errorOperation(error)
        , () => this.nextOperation()
      )
    }
    else {
      this.isLoading = false
      this.makeTable()
    }
    this.createForm()
  }

  createForm() {
    this.filterForm = this.fb.group({
      filter: ['']
    })
  }

  nextOperation() {
    this.isLoading = false
    this.productSvc.setProducts(this.ProductsData)
    this.products = Object.values(this.ProductsData.products).map(prod => prod['name'])
        
    for(let prod of this.products){
      this.product_check[prod] = false
    }
  }

  makeTable() {
    this.tableContent = this.ProductsData['products']
    this.dataSource = new MatTableDataSource<Product>(this.tableContent)
    this.dataSource.paginator = this.paginator
  }

  customFilter(data: Product, name: string): boolean {
    return data['name'] == name
  }

  filterProduct() {    
    this.dataSource.filter = this.filterForm.get('filter').value
  }

  errorOperation(msg: string) {
    this.hasError = true
    this.isLoading = false
    this.errorMsg = msg
  }

  toSentimentDetail(name: string){
      this.getSentimentSubs = this.getSentimentSubs = this.sentimentSvc.getSentiment(name).subscribe(sentiment => this.SentimentData = sentiment, undefined, () => {
      this.sentimentSvc.setSentiObj(this.SentimentData);
      this.router.navigate(['detail']);
    })
  }
  
  private i = 0
  private keys = ['']
  private checked2: string[];
  compare(){
    this.checked = []
    this.keys = Object.keys(this.product_check)
    
    for (let name of this.keys) {
      if(this.product_check[name]){
        this.checked.push(name)
      }
    }

    if (this.checked.length == 0){
      this.products_valid = false
      this.products_error = "Check at least one."
    }
    else if(this.checked.length > 3){
      this.products_valid = false
      this.products_error = "Maximum compare three items."
    }
    else{
      this.products_valid = true;
      // this.productSvc.setChecked(this.checked)
      this.checked_tmp = this.productSvc.getChecked().getValue();
      if(this.checked_tmp != this.checked){
        this.productSvc.setChecked(this.checked);
        this.sentimentSvc.resetSentis();
      }
      this.router.navigate(['compare']);
    }
  }
}
