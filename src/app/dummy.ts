export const samsungS9 = {
    data: [
      {
        raw_text: "My Galaxy S9 Plus does alright! Just waiting for a while to break out the Nikon and the Sony but here's a couple of quickies from around the National Gallery in Ottawa. #samsung… https://t.co/CyQze9rjYP",
        text: [
          "galaxy",
          "s9",
          "plus",
          "alright",
          "waiting",
          "break",
          "nikon",
          "sony",
          "'s",
          "couple",
          "quickies",
          "around",
          "national",
          "gallery",
          "ottawa"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @SamsungCanada: It's here. Get the all-new Samsung Galaxy S9 | S9+ at the #SamsungExperienceStore in @CFtoeatonCentre #GalaxyS9",
        text: [
          "'s",
          "get",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "s9"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @SamsungCanada: It's here. Get the all-new Samsung Galaxy S9 | S9+ at the #SamsungExperienceStore in @CFtoeatonCentre #GalaxyS9",
        text: [
          "'s",
          "get",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "s9"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/pQkjLxONNs",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @STEALSRUS: ⚡️ STEAL via Amazon 📱🔋 WIRELESS CHARGER -10W Fast WIRELESS CHARGING PAD for Samsung Galaxy S9+ S9 S8 Note 8, Standard Wire…",
        text: [
          "steal",
          "via",
          "amazon",
          "wireless",
          "charger",
          "10w",
          "fast",
          "wireless",
          "charging",
          "pad",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "s8",
          "note",
          "8",
          "standard",
          "wire"
        ],
        label: "neg"
      },
      {
        raw_text: "ESSIEN 2 in 1 Car Vent Mount Qi Wireless Charger For Samsung S9 S8 Note 8 Wireless Charging Pad Car Holder Stand For iphone X 8 Tag a friend who would love this! FREE Shipping Worldwide Get it here ---> https://t.co/SgG4rhtc4Q https://t.co/x8E8PqHM41",
        text: [
          "essien",
          "2",
          "1",
          "car",
          "vent",
          "mount",
          "qi",
          "wireless",
          "charger",
          "samsung",
          "s9",
          "s8",
          "note",
          "8",
          "wireless",
          "charging",
          "pad",
          "car",
          "holder",
          "stand",
          "iphone",
          "x",
          "8",
          "tag",
          "friend",
          "would",
          "love",
          "free",
          "shipping",
          "worldwide",
          "get"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/hRBxQYNyu2 #style #fashion #gadgets https://t.co/zyJcsWsTvw",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/8wYkX067TH #style #fashion #gadgets https://t.co/wy2l2zyvUL",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "We've added new Samsung Galaxy S9 SM-G960F/DS Dual SIM 128Gb Titanium Grey - Factory Unlocked at our store. Check it out here: https://t.co/exNg83hIsQ.",
        text: [
          "'ve",
          "added",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "sm",
          "g960f",
          "ds",
          "dual",
          "sim",
          "128gb",
          "titanium",
          "grey",
          "factory",
          "unlocked",
          "store",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "We've added new Samsung Galaxy S9 SM-G960F/DS Dual SIM 128Gb Coral Blue - Factory Unlocked at our store. Check it out here: https://t.co/vllaFMK9Sp.",
        text: [
          "'ve",
          "added",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "sm",
          "g960f",
          "ds",
          "dual",
          "sim",
          "128gb",
          "coral",
          "blue",
          "factory",
          "unlocked",
          "store",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "@AllenDeatherage We'll just leave this here for some light reading... https://t.co/FxIfVcFw9i. 💅",
        text: [
          "'ll",
          "leave",
          "light",
          "reading"
        ],
        label: "neg"
      },
      {
        raw_text: "iPhone X or Samsung s9? I need help here!!",
        text: [
          "iphone",
          "x",
          "samsung",
          "s9",
          "need",
          "help"
        ],
        label: "neg"
      },
      {
        raw_text: "I call bs this article. Samsung's software competition mentioned here are junk. \"Samsung excels...but the native apps for, say, the built-in keyboard, browser and messaging apps aren't as good as Google's Gboard keyboard, Chrome and Android Messages app\" https://t.co/RSJBJNNoSo",
        text: [
          "call",
          "bs",
          "article",
          "samsung",
          "'s",
          "software",
          "competition",
          "mentioned",
          "junk",
          "samsung",
          "excels",
          "native",
          "apps",
          "say",
          "built",
          "keyboard",
          "browser",
          "messaging",
          "apps",
          "good",
          "google",
          "'s",
          "gboard",
          "keyboard",
          "chrome",
          "android",
          "messages",
          "app"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @AppsLeban: Here's a bin on a Chinese forum someone sent me 491196xxx282xx5 07/22 396 Vpn South Korea Card a Samsung Galaxy S9/S9+ @…",
        text: [
          "'s",
          "bin",
          "chinese",
          "forum",
          "someone",
          "sent",
          "491196xxx282xx5",
          "07",
          "22",
          "396",
          "vpn",
          "south",
          "korea",
          "card",
          "samsung",
          "galaxy",
          "s9",
          "s9"
        ],
        label: "neg"
      },
      {
        raw_text: "I can't sleep and I'm just here looking at Samsung S9 pictures. My bi-day 😂 is coming soon, feel free to surprise me😓😓 you don't even have to wait for it sef. Just surprise me😂😓",
        text: [
          "sleep",
          "'m",
          "looking",
          "samsung",
          "s9",
          "pictures",
          "bi",
          "day",
          "coming",
          "soon",
          "feel",
          "free",
          "surprise",
          "even",
          "wait",
          "sef",
          "surprise"
        ],
        label: "pos"
      },
      {
        raw_text: "@ATT been with you peeps for a few years. Suddenly had to move and now I'm barely getting signal. I'm not even in the wilderness. I keep getting emails to upgrade my Samsung S7 and I'd love an S9 but if there's no service here, why?",
        text: [
          "peeps",
          "years",
          "suddenly",
          "move",
          "'m",
          "barely",
          "getting",
          "signal",
          "'m",
          "even",
          "wilderness",
          "keep",
          "getting",
          "emails",
          "upgrade",
          "samsung",
          "s7",
          "'d",
          "love",
          "s9",
          "'s",
          "service"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @Vodacom: Now you can count calories with ease thanks to the #Samsung #Galaxy #S9. Just point your camera at your food & #Bixby will giv…",
        text: [
          "count",
          "calories",
          "ease",
          "thanks",
          "point",
          "camera",
          "food",
          "giv"
        ],
        label: "pos"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/hRBxQYNyu2 #style #fashion #gadgets https://t.co/7lj28MIQRZ",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/8wYkX067TH #style #fashion #gadgets https://t.co/qTsLxLh0zQ",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @AppsLeban: Here's a bin on a Chinese forum someone sent me 491196xxx282xx5 07/22 396 Vpn South Korea Card a Samsung Galaxy S9/S9+ @…",
        text: [
          "'s",
          "bin",
          "chinese",
          "forum",
          "someone",
          "sent",
          "491196xxx282xx5",
          "07",
          "22",
          "396",
          "vpn",
          "south",
          "korea",
          "card",
          "samsung",
          "galaxy",
          "s9",
          "s9"
        ],
        label: "neg"
      },
      {
        raw_text: "Here's a bin on a Chinese forum someone sent me 491196xxx282xx5 07/22 396 Vpn South Korea Card a Samsung Galaxy S9/S9+ @JOKERKILL3R @RayyanThor @iSteal007 @imohammedkha @FlashBins @TheBinGodz @dedo774 @iDang2002 @gamehacker080",
        text: [
          "'s",
          "bin",
          "chinese",
          "forum",
          "someone",
          "sent",
          "491196xxx282xx5",
          "07",
          "22",
          "396",
          "vpn",
          "south",
          "korea",
          "card",
          "samsung",
          "galaxy",
          "s9",
          "s9"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @SamsungCanada: It's here. Get the all-new Samsung Galaxy S9 | S9+ at the #SamsungExperienceStore in @CFtoeatonCentre #GalaxyS9",
        text: [
          "'s",
          "get",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "s9"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/oDVEd43vC8 #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/smIfJaZU0h #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/2LSTq38QvK",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @STEALSRUS: ⚡️ STEAL via Amazon 📱🔋 WIRELESS CHARGER -10W Fast WIRELESS CHARGING PAD for Samsung Galaxy S9+ S9 S8 Note 8, Standard Wire…",
        text: [
          "steal",
          "via",
          "amazon",
          "wireless",
          "charger",
          "10w",
          "fast",
          "wireless",
          "charging",
          "pad",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "s8",
          "note",
          "8",
          "standard",
          "wire"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @vzw786wireless: Watch high quality videos on the fastest smartphone ever. Visit 786Wireless to get the Samsung Galaxy S9 today! See Sto…",
        text: [
          "watch",
          "high",
          "quality",
          "videos",
          "fastest",
          "smartphone",
          "ever",
          "visit",
          "786wireless",
          "get",
          "samsung",
          "galaxy",
          "s9",
          "today",
          "see",
          "sto"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @PitakaGallery: Buy nice or buy twice? Buy nice & once ( guaranteed for lifetime) here: https://t.co/ypVtTcHjkM #everydaycarr…",
        text: [
          "buy",
          "nice",
          "buy",
          "twice",
          "buy",
          "nice",
          "guaranteed",
          "lifetime"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @PitakaGallery: Buy nice or buy twice? Buy nice & once ( guaranteed for lifetime) here: https://t.co/ypVtTcHjkM #everydaycarr…",
        text: [
          "buy",
          "nice",
          "buy",
          "twice",
          "buy",
          "nice",
          "guaranteed",
          "lifetime"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @ismasaleem: Amazon Prime Day Squish Wireless Charger 7.5W for iPhone X/8/8 Plus and 10W for Samsung Galaxy S9/S9 Plus/Note 8/S8/S8 Plus…",
        text: [
          "amazon",
          "prime",
          "day",
          "squish",
          "wireless",
          "charger",
          "7",
          "5w",
          "iphone",
          "x",
          "8",
          "8",
          "plus",
          "10w",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "plus",
          "note",
          "8",
          "s8",
          "s8",
          "plus"
        ],
        label: "neg"
      },
      {
        raw_text: "Amazon Prime Day Squish Wireless Charger 7.5W for iPhone X/8/8 Plus and 10W for Samsung Galaxy S9/S9 Plus/Note 8/S8/S8 Plus Fast Wireless Charging Pad Phone Stand @amazon #amazon #AmazonPrimeDay #AmazonPrime #PrimeDay Buy here 👉 https://t.co/k3kMjVfwmi https://t.co/2Wi6GVNzev",
        text: [
          "amazon",
          "prime",
          "day",
          "squish",
          "wireless",
          "charger",
          "7",
          "5w",
          "iphone",
          "x",
          "8",
          "8",
          "plus",
          "10w",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "plus",
          "note",
          "8",
          "s8",
          "s8",
          "plus",
          "fast",
          "wireless",
          "charging",
          "pad",
          "phone",
          "stand",
          "buy"
        ],
        label: "neg"
      },
      {
        raw_text: "OMG!!! yes! plz!! I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/QJoJbgJDS3",
        text: [
          "omg",
          "yes",
          "plz",
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "RT I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/QJoJbgJDS3",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/4cEy7kiywy",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "Here is the no-so-accurate auto-generated animated emoji from my new work-issued Samsung Galaxy S9 phone. After the camera app takes your selfie (no glasses, pull your hair back) it… https://t.co/88FpMgwiRx",
        text: [
          "accurate",
          "auto",
          "generated",
          "animated",
          "emoji",
          "new",
          "work",
          "issued",
          "samsung",
          "galaxy",
          "s9",
          "phone",
          "camera",
          "app",
          "takes",
          "selfie",
          "glasses",
          "pull",
          "hair",
          "back"
        ],
        label: "neg"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/4Ey32Nt1Q9 #affiliate #livingcooper #amazongiveaway",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/hRBxQYNyu2 #style #fashion #gadgets https://t.co/fbYCKPdklL",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/8wYkX067TH #style #fashion #gadgets https://t.co/zdQXHp5WWW",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @sesameenable: We are here in DC for #RESNA18! Don't miss our first session, Friday from 4 - 5pm. Interested in winning a brand new Sams…",
        text: [
          "dc",
          "miss",
          "first",
          "session",
          "friday",
          "4",
          "5pm",
          "interested",
          "winning",
          "brand",
          "new",
          "sams"
        ],
        label: "pos"
      },
      {
        raw_text: ".@EmperorSearcy said it get down here! Sprint Store at Camp Creek 12:30p-2:30p + we will have free #CarNBike tickets on site! Switch to Sprint and get 50% off a Samsung Galaxy S9 lease. Restrictions apply. #AD https://t.co/K8UnM8iUxJ",
        text: [
          "said",
          "get",
          "sprint",
          "store",
          "camp",
          "creek",
          "12",
          "30p",
          "2",
          "30p",
          "free",
          "tickets",
          "site",
          "switch",
          "sprint",
          "get",
          "50",
          "samsung",
          "galaxy",
          "s9",
          "lease",
          "restrictions",
          "apply"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @STEALSRUS: ⚡️ STEAL via Amazon 📱🔋 WIRELESS CHARGER -10W Fast WIRELESS CHARGING PAD for Samsung Galaxy S9+ S9 S8 Note 8, Standard Wire…",
        text: [
          "steal",
          "via",
          "amazon",
          "wireless",
          "charger",
          "10w",
          "fast",
          "wireless",
          "charging",
          "pad",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "s8",
          "note",
          "8",
          "standard",
          "wire"
        ],
        label: "neg"
      },
      {
        raw_text: "This phone takes wild pics, crystal clear. Check out the Samsung Galaxy S9 at East Com! I'll be here at Quidi Vidi for the next hour. Stop by! https://t.co/1aGPqYul1b",
        text: [
          "phone",
          "takes",
          "wild",
          "pics",
          "crystal",
          "clear",
          "check",
          "samsung",
          "galaxy",
          "s9",
          "east",
          "com",
          "'ll",
          "quidi",
          "vidi",
          "next",
          "hour",
          "stop"
        ],
        label: "pos"
      },
      {
        raw_text: "⚡️ STEAL via Amazon 📱🔋 WIRELESS CHARGER -10W Fast WIRELESS CHARGING PAD for Samsung Galaxy S9+ S9 S8 Note 8, Standard Wireless Charge for iPhone and All Qi-Enabled Phones ONLY $12.79💥 FREE ONE DAY Shipping w PRIME (Retail $15.99) Shop Here 👉 https://t.co/iEdMLBxFPV https://t.co/ntEiqzhexB",
        text: [
          "steal",
          "via",
          "amazon",
          "wireless",
          "charger",
          "10w",
          "fast",
          "wireless",
          "charging",
          "pad",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "s8",
          "note",
          "8",
          "standard",
          "wireless",
          "charge",
          "iphone",
          "qi",
          "enabled",
          "phones",
          "12",
          "79",
          "free",
          "one",
          "day",
          "shipping",
          "w",
          "prime",
          "retail",
          "15",
          "99",
          "shop"
        ],
        label: "neg"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/QJoJbgJDS3",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "Luxury 360 Degree Full Cover Phone Shockproof Case For Samsung Galaxy S9 S8 Plus – PURCHASE HERE https://t.co/lextauFjhA",
        text: [
          "luxury",
          "360",
          "degree",
          "full",
          "cover",
          "phone",
          "shockproof",
          "case",
          "samsung",
          "galaxy",
          "s9",
          "s8",
          "plus",
          "purchase"
        ],
        label: "neg"
      },
      {
        raw_text: "Luxury 360 Degree Full Cover Phone Shockproof Case For Samsung Galaxy S9 S8 Plus – PURCHASE HERE https://t.co/5c6GRlikQS",
        text: [
          "luxury",
          "360",
          "degree",
          "full",
          "cover",
          "phone",
          "shockproof",
          "case",
          "samsung",
          "galaxy",
          "s9",
          "s8",
          "plus",
          "purchase"
        ],
        label: "neg"
      },
      {
        raw_text: "It's finally raining in South #Delhi. Here's a Super Slow-Motion video courtesy Samsung's Galaxy S9 Plus! https://t.co/78XZHKoBM0",
        text: [
          "'s",
          "finally",
          "raining",
          "south",
          "'s",
          "super",
          "slow",
          "motion",
          "video",
          "courtesy",
          "samsung",
          "'s",
          "galaxy",
          "s9",
          "plus"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @Vodacom: Now you can count calories with ease thanks to the #Samsung #Galaxy #S9. Just point your camera at your food & #Bixby will giv…",
        text: [
          "count",
          "calories",
          "ease",
          "thanks",
          "point",
          "camera",
          "food",
          "giv"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @Vodacom: Now you can count calories with ease thanks to the #Samsung #Galaxy #S9. Just point your camera at your food & #Bixby will giv…",
        text: [
          "count",
          "calories",
          "ease",
          "thanks",
          "point",
          "camera",
          "food",
          "giv"
        ],
        label: "pos"
      },
      {
        raw_text: "Now you can count calories with ease thanks to the #Samsung #Galaxy #S9. Just point your camera at your food & #Bixby will give you all the nutritional information you need. Order yours here: https://t.co/I5Nuo3I6rf https://t.co/AGKxKePcSZ",
        text: [
          "count",
          "calories",
          "ease",
          "thanks",
          "point",
          "camera",
          "food",
          "give",
          "nutritional",
          "information",
          "need",
          "order"
        ],
        label: "pos"
      },
      {
        raw_text: "Now available on our store: Ugreen Mobile Pho... Check it out here! https://t.co/1m46HQ3unB",
        text: [
          "available",
          "store",
          "ugreen",
          "mobile",
          "pho",
          "check"
        ],
        label: "neg"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/QJoJbgJDS3",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "We are here in DC for #RESNA18! Don't miss our first session, Friday from 4 - 5pm. Interested in winning a brand new Samsung S9+ with a lifetime Open Sesame license? Come by our booth (#295) and participate in our raffle, and you'll have a chance at being our one lucky winner!",
        text: [
          "dc",
          "miss",
          "first",
          "session",
          "friday",
          "4",
          "5pm",
          "interested",
          "winning",
          "brand",
          "new",
          "samsung",
          "s9",
          "lifetime",
          "open",
          "sesame",
          "license",
          "come",
          "booth",
          "participate",
          "raffle",
          "'ll",
          "chance",
          "one",
          "lucky",
          "winner"
        ],
        label: "pos"
      },
      {
        raw_text: "@allan_buxton @sprint However, you need to get a comparable SIM card because the Nexus 6P's SIM card is not comparable with the Samsung Galaxy S9. You can locate your nearest Sprint store here: https://t.co/lcKpzwiPK5, so they can help you to get a compatible SIM. (2/2) - ER",
        text: [
          "however",
          "need",
          "get",
          "comparable",
          "sim",
          "card",
          "nexus",
          "6p",
          "'s",
          "sim",
          "card",
          "comparable",
          "samsung",
          "galaxy",
          "s9",
          "locate",
          "nearest",
          "sprint",
          "store",
          "help",
          "get",
          "compatible",
          "sim",
          "2",
          "2",
          "er"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @SamsungCanada: It's here. Get the all-new Samsung Galaxy S9 | S9+ at the #SamsungExperienceStore in @CFtoeatonCentre #GalaxyS9",
        text: [
          "'s",
          "get",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "s9"
        ],
        label: "pos"
      },
      {
        raw_text: "Is it worth upgrading to the Samsung Galaxy S9 if you are an existing Galaxy handset owner, particularly an owner of the S8? Heres our expert opinion https://t.co/K7TZTcvK3J https://t.co/QiKmgMcfry",
        text: [
          "worth",
          "upgrading",
          "samsung",
          "galaxy",
          "s9",
          "existing",
          "galaxy",
          "handset",
          "owner",
          "particularly",
          "owner",
          "s8",
          "expert",
          "opinion"
        ],
        label: "pos"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/hRBxQYNyu2 #style #fashion #gadgets https://t.co/qic97BJBVL",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/8wYkX067TH #style #fashion #gadgets https://t.co/SMsfOfAy6y",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "The tech giant is letting customers save up to PKR 10,000 on their purchase! Here's the new price list: https://t.co/GJqyIZ8ye0 https://t.co/h4RQE2pxUp",
        text: [
          "tech",
          "giant",
          "letting",
          "customers",
          "save",
          "pkr",
          "10",
          "000",
          "purchase",
          "'s",
          "new",
          "price",
          "list"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @stealstreetwear: ⚡️ STEALS via Amazon - ENDS SOON! 🔋📱 WIRELESS DUAL USB POWER BANK 8000mAh (For iPhone X/8/8 Plus,Samsung Galaxy S9/…",
        text: [
          "steals",
          "via",
          "amazon",
          "ends",
          "soon",
          "wireless",
          "dual",
          "usb",
          "power",
          "bank",
          "8000mah",
          "iphone",
          "x",
          "8",
          "8",
          "plus",
          "samsung",
          "galaxy",
          "s9"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @stealstreetwear: ⚡️ STEALS via Amazon - ENDS SOON! 🔋📱 WIRELESS DUAL USB POWER BANK 8000mAh (For iPhone X/8/8 Plus,Samsung Galaxy S9/…",
        text: [
          "steals",
          "via",
          "amazon",
          "ends",
          "soon",
          "wireless",
          "dual",
          "usb",
          "power",
          "bank",
          "8000mah",
          "iphone",
          "x",
          "8",
          "8",
          "plus",
          "samsung",
          "galaxy",
          "s9"
        ],
        label: "neg"
      },
      {
        raw_text: "⚡️ STEALS via Amazon - ENDS SOON! 🔋📱 WIRELESS DUAL USB POWER BANK 8000mAh (For iPhone X/8/8 Plus,Samsung Galaxy S9/S8/S7/S6 Edge+/Note8 all Mobile Phone Qi-enabled) ONLY $13.99💥 (Retail $29.99) Buy Here 👉 https://t.co/Y9J8s6huLc https://t.co/9xskxFxPLy",
        text: [
          "steals",
          "via",
          "amazon",
          "ends",
          "soon",
          "wireless",
          "dual",
          "usb",
          "power",
          "bank",
          "8000mah",
          "iphone",
          "x",
          "8",
          "8",
          "plus",
          "samsung",
          "galaxy",
          "s9",
          "s8",
          "s7",
          "s6",
          "edge",
          "note8",
          "mobile",
          "phone",
          "qi",
          "enabled",
          "13",
          "99",
          "retail",
          "29",
          "99",
          "buy"
        ],
        label: "neg"
      },
      {
        raw_text: "They're finally here — Samsung has officially revealed the Galaxy S9 and S9+, which run Android Oreo out of the box. Since we've... https://t.co/O1qG689bjo",
        text: [
          "'re",
          "finally",
          "samsung",
          "officially",
          "revealed",
          "galaxy",
          "s9",
          "s9",
          "run",
          "android",
          "oreo",
          "box",
          "since",
          "'ve"
        ],
        label: "pos"
      },
      {
        raw_text: "Buy nice or buy twice? Buy nice & once ( guaranteed for lifetime) here: https://t.co/ypVtTcHjkM #everydaycarry https://t.co/4V0SnuFerN",
        text: [
          "buy",
          "nice",
          "buy",
          "twice",
          "buy",
          "nice",
          "guaranteed",
          "lifetime"
        ],
        label: "pos"
      },
      {
        raw_text: "Samsung Galaxy S8/S8+ and Note 8 update adds screen rotation option After bringing video lockscreen option from Galaxy S9 to Galaxy S8/S8+ & Note 8, Samsung plans another feature to those launched last year. Samsung Galaxy S9 is only... Read More Here—> https://t.co/vvMm9FyZyD https://t.co/cebGmYjvqP",
        text: [
          "samsung",
          "galaxy",
          "s8",
          "s8",
          "note",
          "8",
          "update",
          "adds",
          "screen",
          "rotation",
          "option",
          "bringing",
          "video",
          "lockscreen",
          "option",
          "galaxy",
          "s9",
          "galaxy",
          "s8",
          "s8",
          "note",
          "8",
          "samsung",
          "plans",
          "another",
          "feature",
          "launched",
          "last",
          "year",
          "samsung",
          "galaxy",
          "s9",
          "read"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @biggsytravels: When you know you've gone a bit OTT with your Snapseed settings but you can't stop playing around with your loaned Samsu…",
        text: [
          "know",
          "'ve",
          "gone",
          "bit",
          "ott",
          "snapseed",
          "settings",
          "stop",
          "playing",
          "around",
          "loaned",
          "samsu"
        ],
        label: "neg"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/xZ0nUoDqD1 #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @Barclays_Kenya: Week 2 is here and yes... we do have some goodies on our mystery day. We will give you a brand new Samsung S9. Simply…",
        text: [
          "week",
          "2",
          "yes",
          "goodies",
          "mystery",
          "day",
          "give",
          "brand",
          "new",
          "samsung",
          "s9",
          "simply"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @Barclays_Kenya: Week 2 is here and yes... we do have some goodies on our mystery day. We will give you a brand new Samsung S9. Simply…",
        text: [
          "week",
          "2",
          "yes",
          "goodies",
          "mystery",
          "day",
          "give",
          "brand",
          "new",
          "samsung",
          "s9",
          "simply"
        ],
        label: "pos"
      },
      {
        raw_text: "Like and Share if you want this YILIZOMANA Qi Wireless Charger Charging Pad for iPhone 8 8 Plus X Samsung Note 8 Galaxy S8,S8 Plus,S7,S9,S6 Edge+ Fast charging Tag a friend who would love this! FREE Shipping Worldwide Get it here ---> https://t.co/85pW5P5Lpd https://t.co/l72MPXH5H6",
        text: [
          "like",
          "share",
          "want",
          "yilizomana",
          "qi",
          "wireless",
          "charger",
          "charging",
          "pad",
          "iphone",
          "8",
          "8",
          "plus",
          "x",
          "samsung",
          "note",
          "8",
          "galaxy",
          "s8",
          "s8",
          "plus",
          "s7",
          "s9",
          "s6",
          "edge",
          "fast",
          "charging",
          "tag",
          "friend",
          "would",
          "love",
          "free",
          "shipping",
          "worldwide",
          "get"
        ],
        label: "neg"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/H8Bi0H4y6Y #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "When you know you've gone a bit OTT with your Snapseed settings but you can't stop playing around with your loaned Samsung S9+ :) Here's the brutal Shakespeare Tower on The Barbican Estate #ThreeGallery #GoRoam https://t.co/MCzb4Yi6Th",
        text: [
          "know",
          "'ve",
          "gone",
          "bit",
          "ott",
          "snapseed",
          "settings",
          "stop",
          "playing",
          "around",
          "loaned",
          "samsung",
          "s9",
          "'s",
          "brutal",
          "shakespeare",
          "tower",
          "barbican",
          "estate"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/hRBxQYNyu2 #style #fashion #gadgets https://t.co/pShwww5OpM",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/8wYkX067TH #style #fashion #gadgets",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "This Case Brings Samsung Galaxy S9 Clear View Standing Cover-Like Effect To iPhone - https://t.co/WYlSvenRpj out this case which brings Samsung Galaxy S9 Clear View Standing Cover-like effect to your iPhone, and best of all, it's on sale an can be grabbed from here right away.... https://t.co/HJ54rE7B1L",
        text: [
          "case",
          "brings",
          "samsung",
          "galaxy",
          "s9",
          "clear",
          "view",
          "standing",
          "cover",
          "like",
          "effect",
          "iphone",
          "case",
          "brings",
          "samsung",
          "galaxy",
          "s9",
          "clear",
          "view",
          "standing",
          "cover",
          "like",
          "effect",
          "iphone",
          "best",
          "'s",
          "sale",
          "grabbed",
          "right",
          "away"
        ],
        label: "pos"
      },
      {
        raw_text: "This Case Brings Samsung Galaxy S9 Clear View Standing Cover-Like Effect To iPhone. Check out this case which brings Samsung Galaxy S9 Clear View Standing Cover-like effect to your iPhone, and best of all, it's on sale an can be grabbed from here r... https://t.co/mnzu0n1JLC https://t.co/2GZp2f3Zfo",
        text: [
          "case",
          "brings",
          "samsung",
          "galaxy",
          "s9",
          "clear",
          "view",
          "standing",
          "cover",
          "like",
          "effect",
          "iphone",
          "check",
          "case",
          "brings",
          "samsung",
          "galaxy",
          "s9",
          "clear",
          "view",
          "standing",
          "cover",
          "like",
          "effect",
          "iphone",
          "best",
          "'s",
          "sale",
          "grabbed",
          "r"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @Barclays_Kenya: Week 2 is here and yes... we do have some goodies on our mystery day. We will give you a brand new Samsung S9. Simply…",
        text: [
          "week",
          "2",
          "yes",
          "goodies",
          "mystery",
          "day",
          "give",
          "brand",
          "new",
          "samsung",
          "s9",
          "simply"
        ],
        label: "pos"
      },
      {
        raw_text: "Here's how you can install TWRP, disable encryption, and root Samsung Galaxy S9/S9+ easily. The tutorial takes you through the process in detail - https://t.co/gamFtPpCfN #SamsungGalaxyS9Plus",
        text: [
          "'s",
          "install",
          "twrp",
          "disable",
          "encryption",
          "root",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "easily",
          "tutorial",
          "takes",
          "process",
          "detail"
        ],
        label: "neg"
      },
      {
        raw_text: "@AlmightyUtre Super Slow-mo, Dual Aperture, Stereo Speakers to name a few. Check it out here: https://t.co/FxIfVcFw9i",
        text: [
          "super",
          "slow",
          "mo",
          "dual",
          "aperture",
          "stereo",
          "speakers",
          "name",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/pQkjLxONNs",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/UaHYeqr3c4 #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "@strikegroup We have a similar solution here in the UK using a Strike phone dock. Features 12\" Touchscreen, Strike dock, VESA & strike mounts for securing to dash/cetre console, etc. For @samsungbusiness @samsungbusiness @Samsung #DeX enabled handsets - S8/S8+, S9/S9+ and Note 8. https://t.co/PbsD5eTOt2",
        text: [
          "similar",
          "solution",
          "uk",
          "using",
          "strike",
          "phone",
          "dock",
          "features",
          "12",
          "touchscreen",
          "strike",
          "dock",
          "vesa",
          "strike",
          "mounts",
          "securing",
          "dash",
          "cetre",
          "console",
          "etc",
          "enabled",
          "handsets",
          "s8",
          "s8",
          "s9",
          "s9",
          "note",
          "8"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @mikeymushi: Enter the Facts Africa season finale GalaxyS9 Kaspersky #Giveaway here https://t.co/hfttYrmj9e",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "galaxys9",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Here is a good new for Samsung family that Samsung store now open at Ground Floor, The Centaurus Mall. #samsung #official #outlet #electronics #mobile #phones #s9 #s8 #A #J4 #mobile #worldclass #bestphones #digitalworld #thecentaurusmall https://t.co/H2YKsQWpK5",
        text: [
          "good",
          "new",
          "samsung",
          "family",
          "samsung",
          "store",
          "open",
          "ground",
          "floor",
          "centaurus",
          "mall"
        ],
        label: "neg"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/ShPtJWYiDf",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "@King_La670 Apple user here since the iPhone 4. I am making the switch to Samsung unless Apple completely steps it up with an affordable base price that is $999. Currently have the 7+ and plan to switch to the Galaxy S9. Apple storage bad, cloud storage costs too much. + no SD slots.",
        text: [
          "apple",
          "user",
          "since",
          "iphone",
          "4",
          "making",
          "switch",
          "samsung",
          "unless",
          "apple",
          "completely",
          "steps",
          "affordable",
          "base",
          "price",
          "999",
          "currently",
          "7",
          "plan",
          "switch",
          "galaxy",
          "s9",
          "apple",
          "storage",
          "bad",
          "cloud",
          "storage",
          "costs",
          "much",
          "sd",
          "slots"
        ],
        label: "neg"
      },
      {
        raw_text: "#RESNA18 here we come! We will be displaying our assistive technology as well as participating in 2 sessions on Friday and Sunday. Definitely stop by our booth (295) where we are having a raffle for a BRAND NEW SAMSUNG S9+! Looking forward to the opportunity and meeting everyone! https://t.co/SRoZc7YaD0",
        text: [
          "come",
          "displaying",
          "assistive",
          "technology",
          "well",
          "participating",
          "2",
          "sessions",
          "friday",
          "sunday",
          "definitely",
          "stop",
          "booth",
          "295",
          "raffle",
          "brand",
          "new",
          "samsung",
          "s9",
          "looking",
          "forward",
          "opportunity",
          "meeting",
          "everyone"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/7fQ1ZYse4C",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/Y2dCaGgJiw #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "RT I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/QJoJbgJDS3",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "Andy and Sherry like most of the Samsung Galaxy S9+ a lot, here's why #samsung #s9 #getconnected https://t.co/SR28IHwteJ",
        text: [
          "andy",
          "sherry",
          "like",
          "samsung",
          "galaxy",
          "s9",
          "lot",
          "'s"
        ],
        label: "pos"
      },
      {
        raw_text: "YIESOM Type C Anti Dust Plug Set USB Type-C Port and 3.5mm Earphone Jack Plug For Samsung Galaxy Note 8 S8 S9 Plus Oneplus 5 5T FREE Shipping Worldwide Get it here ---> https://t.co/yK89RvetL2 https://t.co/MDev54ORfv",
        text: [
          "yiesom",
          "type",
          "c",
          "anti",
          "dust",
          "plug",
          "set",
          "usb",
          "type",
          "c",
          "port",
          "3",
          "5mm",
          "earphone",
          "jack",
          "plug",
          "samsung",
          "galaxy",
          "note",
          "8",
          "s8",
          "s9",
          "plus",
          "oneplus",
          "5",
          "5t",
          "free",
          "shipping",
          "worldwide",
          "get"
        ],
        label: "neg"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/rCs1kzWKh6",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/pQkjLxONNs",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/5GGUOwlvlH",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/fozsyUphRp",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "5G ISN'T HERE YET, but Telefonica is bringing a nearly-there equivalent in the form of 4.9G LTE which it's bringing to Samsung phones with a little help from Nokia. Both phone makers... https://t.co/4VwDqFrAzU",
        text: [
          "5g",
          "yet",
          "telefonica",
          "bringing",
          "nearly",
          "equivalent",
          "form",
          "4",
          "9g",
          "lte",
          "'s",
          "bringing",
          "samsung",
          "phones",
          "little",
          "help",
          "nokia",
          "phone",
          "makers"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/2Uwv2kjzsn #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/iF0gMDkGHs",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @amitbhawani: I have just taken the Blind Test between OnePlus 6, iPhone X, Samsung Galaxy S9 and Google Pixel 2. Vote here https://t.…",
        text: [
          "taken",
          "blind",
          "test",
          "oneplus",
          "6",
          "iphone",
          "x",
          "samsung",
          "galaxy",
          "s9",
          "google",
          "pixel",
          "2",
          "vote"
        ],
        label: "neg"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/b0qR2ZDBRD #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/Kgf0HS97us #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/MBY1vw4VA5",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @Barclays_Kenya: Week 2 is here and yes... we do have some goodies on our mystery day. We will give you a brand new Samsung S9. Simply…",
        text: [
          "week",
          "2",
          "yes",
          "goodies",
          "mystery",
          "day",
          "give",
          "brand",
          "new",
          "samsung",
          "s9",
          "simply"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/sDl6cEl5ME",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/ShPtJWYiDf",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "I just participated in an #AmazonGiveaway for Samsung Galaxy S9 Unlocked Smartphone.... Check it out here: https://t.co/oxcCTtMiWS",
        text: [
          "participated",
          "samsung",
          "galaxy",
          "s9",
          "unlocked",
          "smartphone",
          "check"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @MugureLynn: Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/c2PQnDBHKL #FactsAfrica #2nacheki #Ka…",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/c2PQnDBHKL #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Is it worth upgrading to the Samsung Galaxy S9 if you are an existing Galaxy handset owner, particularly an owner of the S8? Heres our expert opinion https://t.co/x7XLhhNCGt https://t.co/SgpCIpfk7g",
        text: [
          "worth",
          "upgrading",
          "samsung",
          "galaxy",
          "s9",
          "existing",
          "galaxy",
          "handset",
          "owner",
          "particularly",
          "owner",
          "s8",
          "expert",
          "opinion"
        ],
        label: "pos"
      },
      {
        raw_text: "They're finally here — Samsung has officially revealed the Galaxy S9 and S9+, which run Android Oreo out of the box. Since we've spent plenty of time with the Oreo beta for the Galaxy S8, we already know about some of https://t.co/O1qG68qMaW",
        text: [
          "'re",
          "finally",
          "samsung",
          "officially",
          "revealed",
          "galaxy",
          "s9",
          "s9",
          "run",
          "android",
          "oreo",
          "box",
          "since",
          "'ve",
          "spent",
          "plenty",
          "time",
          "oreo",
          "beta",
          "galaxy",
          "s8",
          "already",
          "know"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/vpeAjvNGuz #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/yrT3jCFrh9 #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "3D Full Cover Screen Protector Glass For Samsung Galaxy S9 S8 Plus S7 S6 edge Tempered Glass For samsung J5 J7 2016 2017 Note 8 FREE Shipping Worldwide Get it here ---> https://t.co/rKw3qjCneZ https://t.co/KDUrD4MSoe",
        text: [
          "3d",
          "full",
          "cover",
          "screen",
          "protector",
          "glass",
          "samsung",
          "galaxy",
          "s9",
          "s8",
          "plus",
          "s7",
          "s6",
          "edge",
          "tempered",
          "glass",
          "samsung",
          "j5",
          "j7",
          "2016",
          "2017",
          "note",
          "8",
          "free",
          "shipping",
          "worldwide",
          "get"
        ],
        label: "neg"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/C8jP1zkJUW #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/v8t3yLzUYw #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @amitbhawani: I have just taken the Blind Test between OnePlus 6, iPhone X, Samsung Galaxy S9 and Google Pixel 2. Vote here https://t.…",
        text: [
          "taken",
          "blind",
          "test",
          "oneplus",
          "6",
          "iphone",
          "x",
          "samsung",
          "galaxy",
          "s9",
          "google",
          "pixel",
          "2",
          "vote"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @SamsungMobileIN: Let darkness be the canvas, and let light be your paint when you create ‘The Halo' effect of the #GalaxyS9. #OwnTheNig…",
        text: [
          "let",
          "darkness",
          "canvas",
          "let",
          "light",
          "paint",
          "create",
          "halo",
          "'",
          "effect"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @SamsungMobileIN: Your imagination lights up full circle with the #GalaxyS9. #OwnTheNight with #LowLightPhotography. Buy now and avail u…",
        text: [
          "imagination",
          "lights",
          "full",
          "circle",
          "buy",
          "avail",
          "u"
        ],
        label: "neg"
      },
      {
        raw_text: "Give our game a play test Zen Block a relaxing game for #Android Sign up and download our demo here https://t.co/UlplGgSPdm #fashion #style #beauty #jewelry #sale #model #news #girls #fashionshow #designer #samsung #s8 #s9plus #s9 https://t.co/njCB2CXPzd",
        text: [
          "give",
          "game",
          "play",
          "test",
          "zen",
          "block",
          "relaxing",
          "game",
          "sign",
          "download",
          "demo"
        ],
        label: "pos"
      },
      {
        raw_text: "So many offers available here at Sprint! The savings just don't stop... Our Samsung S8 and S9 are also 50% off. Stop over paying and come see us today!!! #Sprint #WhyPayMore #Samsung https://t.co/ae0gQexygW",
        text: [
          "many",
          "offers",
          "available",
          "sprint",
          "savings",
          "stop",
          "samsung",
          "s8",
          "s9",
          "also",
          "50",
          "stop",
          "paying",
          "come",
          "see",
          "us",
          "today"
        ],
        label: "neg"
      },
      {
        raw_text: "Enter the Facts Africa Season finale #GalaxyS9 Kaspersky #giveaway here https://t.co/Xi4cKioQgC #FactsAfrica #2nacheki #Kaspersky",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "@RM_Censor @GriffinTech @mobilefun Nothing to announce regarding Extreme for S9+, but we do have several other Survivor cases available for your phone. Check them out here: https://t.co/VR57s1ksnQ",
        text: [
          "nothing",
          "announce",
          "regarding",
          "extreme",
          "s9",
          "several",
          "survivor",
          "cases",
          "available",
          "phone",
          "check"
        ],
        label: "neg"
      },
      {
        raw_text: "Enter the Facts Africa season finale GalaxyS9 Kaspersky #Giveaway here https://t.co/MuttfEbp1j",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "galaxys9",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa season finale GalaxyS9 Kaspersky #Giveaway here https://t.co/hfttYrmj9e",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "galaxys9",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa season finale GalaxyS9 Kaspersky #Giveaway here https://t.co/bVr2HZhsfz",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "galaxys9",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "Enter the Facts Africa season finale GalaxyS9 Kaspersky #Giveaway here https://t.co/3HdBEhtYp5",
        text: [
          "enter",
          "facts",
          "africa",
          "season",
          "finale",
          "galaxys9",
          "kaspersky"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @TrophyHusbandD: Here is the first article Google suggested on my new Samsung Galaxy S9+... Thanks folks. https://t.co/WTDADDp508",
        text: [
          "first",
          "article",
          "google",
          "suggested",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "thanks",
          "folks"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @TrophyHusbandD: Here is the first article Google suggested on my new Samsung Galaxy S9+... Thanks folks. https://t.co/WTDADDp508",
        text: [
          "first",
          "article",
          "google",
          "suggested",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "thanks",
          "folks"
        ],
        label: "pos"
      },
      {
        raw_text: "After this year's launch of the Galaxy S9 and S9 Plus, Samsung is now shifting its focus to the Galaxy Note 9. Here's everything that's worth knowing about the upcoming Samsung Galaxy Note 9. @aadee_ram https://t.co/efmO84rhXb",
        text: [
          "year",
          "'s",
          "launch",
          "galaxy",
          "s9",
          "s9",
          "plus",
          "samsung",
          "shifting",
          "focus",
          "galaxy",
          "note",
          "9",
          "'s",
          "everything",
          "'s",
          "worth",
          "knowing",
          "upcoming",
          "samsung",
          "galaxy",
          "note",
          "9"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @Barclays_Kenya: Week 2 is here and yes... we do have some goodies on our mystery day. We will give you a brand new Samsung S9. Simply…",
        text: [
          "week",
          "2",
          "yes",
          "goodies",
          "mystery",
          "day",
          "give",
          "brand",
          "new",
          "samsung",
          "s9",
          "simply"
        ],
        label: "pos"
      },
      {
        raw_text: "@Mrwhosetheboss @SamsungUK @SamsungMobile @Samsung @SamsungMobileUS @SamsungUS @SamsungMobileIN @SamsungSupport S9+ owner here. Anyone else???",
        text: [
          "s9",
          "owner",
          "anyone",
          "else"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @amitbhawani: Bad news for @SamsungMobile Galaxy S9 and S9+ Owners Some devices are randomly sending your camera roll photos to your co…",
        text: [
          "bad",
          "news",
          "galaxy",
          "s9",
          "s9",
          "owners",
          "devices",
          "randomly",
          "sending",
          "camera",
          "roll",
          "photos",
          "co"
        ],
        label: "neg"
      },
      {
        raw_text: "Week 2 is here and yes... we do have some goodies on our mystery day. We will give you a brand new Samsung S9. Simply be the 50th person to purchase Insurance using the Timiza App on our mystery day this week. In 5…4…3… #TimizaKE T&Cs apply https://t.co/hPfSQ55TPN https://t.co/WmHjsB8ayk",
        text: [
          "week",
          "2",
          "yes",
          "goodies",
          "mystery",
          "day",
          "give",
          "brand",
          "new",
          "samsung",
          "s9",
          "simply",
          "50th",
          "person",
          "purchase",
          "insurance",
          "using",
          "timiza",
          "app",
          "mystery",
          "day",
          "week",
          "5",
          "4",
          "3",
          "cs",
          "apply"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @TrophyHusbandD: Here is the first article Google suggested on my new Samsung Galaxy S9+... Thanks folks. https://t.co/WTDADDp508",
        text: [
          "first",
          "article",
          "google",
          "suggested",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "thanks",
          "folks"
        ],
        label: "pos"
      },
      {
        raw_text: "Compare #SamsungGalaxyNote8 with #SamsungGalaxyS9Plus w.r.t specifications, features, score, prices and Pros & cons here: https://t.co/NXAg95cAdb https://t.co/iAvsdsgyYw",
        text: [
          "compare",
          "w",
          "r",
          "specifications",
          "features",
          "score",
          "prices",
          "pros",
          "cons"
        ],
        label: "pos"
      },
      {
        raw_text: "New post (Samsung Galaxy S9 colors: here are all of the likely color options) has been published on https://t.co/3btBxg1oAV - https://t.co/yegWatOjP9 https://t.co/jOhkakNVaz",
        text: [
          "new",
          "post",
          "samsung",
          "galaxy",
          "s9",
          "colors",
          "likely",
          "color",
          "options",
          "published"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @cv_tech1: I was SHOCKED when I found this out about my Samsung Galaxy S9!!! Check out my YouTube video here > https://t.co/P7YMRI5qdo #…",
        text: [
          "shocked",
          "found",
          "samsung",
          "galaxy",
          "s9",
          "check",
          "youtube",
          "video"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "I was SHOCKED when I found this out about my Samsung Galaxy S9!!! Check out my YouTube video here > https://t.co/P7YMRI5qdo #samsungs9 #samsung #GalaxyS9 #YouTube #YouTuber #samsunggalaxy https://t.co/6h1iltWjGo",
        text: [
          "shocked",
          "found",
          "samsung",
          "galaxy",
          "s9",
          "check",
          "youtube",
          "video"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @TrophyHusbandD: Here is the first article Google suggested on my new Samsung Galaxy S9+... Thanks folks. https://t.co/WTDADDp508",
        text: [
          "first",
          "article",
          "google",
          "suggested",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "thanks",
          "folks"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @TrophyHusbandD: Here is the first article Google suggested on my new Samsung Galaxy S9+... Thanks folks. https://t.co/WTDADDp508",
        text: [
          "first",
          "article",
          "google",
          "suggested",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "thanks",
          "folks"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @TrophyHusbandD: Here is the first article Google suggested on my new Samsung Galaxy S9+... Thanks folks. https://t.co/WTDADDp508",
        text: [
          "first",
          "article",
          "google",
          "suggested",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "thanks",
          "folks"
        ],
        label: "pos"
      },
      {
        raw_text: "Here is the first article Google suggested on my new Samsung Galaxy S9+... Thanks folks. https://t.co/WTDADDp508",
        text: [
          "first",
          "article",
          "google",
          "suggested",
          "new",
          "samsung",
          "galaxy",
          "s9",
          "thanks",
          "folks"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @annwxr_e: do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "do you ever need another reason to buy a new samsung s8/s9? here you have one https://t.co/KbHr1t8e3Q",
        text: [
          "ever",
          "need",
          "another",
          "reason",
          "buy",
          "new",
          "samsung",
          "s8",
          "s9",
          "one"
        ],
        label: "neg"
      },
      {
        raw_text: "Magisk 16.6 Beta and Magisk Manager 5.8.0 comes with improved support for Treble-enabled devices, fixes the root loss issue, adds support for Samsung Galaxy S9/S9+ and a lot more. More details, changelog, and instructions here - https://t.co/q79olS6FvC #Magisk",
        text: [
          "magisk",
          "16",
          "6",
          "beta",
          "magisk",
          "manager",
          "5",
          "8",
          "0",
          "comes",
          "improved",
          "support",
          "treble",
          "enabled",
          "devices",
          "fixes",
          "root",
          "loss",
          "issue",
          "adds",
          "support",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "lot",
          "details",
          "changelog",
          "instructions"
        ],
        label: "pos"
      },
      {
        raw_text: "@doculmus @daringfireball The problem here, is not Samsung sropped 40%, but their Flagship model; S9 possibly excluding S9+ had lower shipment then S7 ( When it didn't had a + counterpart ), The same is likely true, iPhone X dropped x% compared to iPhone 6s Plus.",
        text: [
          "problem",
          "samsung",
          "sropped",
          "40",
          "flagship",
          "model",
          "s9",
          "possibly",
          "excluding",
          "s9",
          "lower",
          "shipment",
          "s7",
          "counterpart",
          "likely",
          "true",
          "iphone",
          "x",
          "dropped",
          "x",
          "compared",
          "iphone",
          "6s",
          "plus"
        ],
        label: "neg"
      },
      {
        raw_text: "Loving the new Samsung s9. Just need my case to get here so I won't be so scared to use it lol. Come on Monday.",
        text: [
          "loving",
          "new",
          "samsung",
          "s9",
          "need",
          "case",
          "get",
          "scared",
          "use",
          "lol",
          "come",
          "monday"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/hRBxQYNyu2 #style #fashion #gadgets https://t.co/rWdtV5lsBM",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "Samsung Galaxy S9 and S9+ - custom cases are here! https://t.co/8wYkX067TH #style #fashion #gadgets https://t.co/URZiQ7kprv",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "custom",
          "cases"
        ],
        label: "neg"
      },
      {
        raw_text: "Easy One Touch 2 Car Mount Universal Phone Holder for iPhone X 8/8 Plus 7 7 Plus 6s Plus 6s 6 SE Samsung Galaxy S9 S9 Plus S8 Plus S8 Edge S7 S6 Note 8 5 $16.95 w/Prime Shop Here: https://t.co/Wb7DeczF8P https://t.co/t4gGGYOPH8",
        text: [
          "easy",
          "one",
          "touch",
          "2",
          "car",
          "mount",
          "universal",
          "phone",
          "holder",
          "iphone",
          "x",
          "8",
          "8",
          "plus",
          "7",
          "7",
          "plus",
          "6s",
          "plus",
          "6s",
          "6",
          "se",
          "samsung",
          "galaxy",
          "s9",
          "s9",
          "plus",
          "s8",
          "plus",
          "s8",
          "edge",
          "s7",
          "s6",
          "note",
          "8",
          "5",
          "16",
          "95",
          "w",
          "prime",
          "shop"
        ],
        label: "neg"
      },
      {
        raw_text: "They're finally here — Samsung has officially revealed the Galaxy S9 and S9+, which run Android Oreo out of the box. Since we've... https://t.co/O1qG689bjo",
        text: [
          "'re",
          "finally",
          "samsung",
          "officially",
          "revealed",
          "galaxy",
          "s9",
          "s9",
          "run",
          "android",
          "oreo",
          "box",
          "since",
          "'ve"
        ],
        label: "pos"
      },
      {
        raw_text: "Apple's iPhone 8 is already outselling Samsung's new Galaxy S9 and S9+ ~ Visit Here: https://t.co/oOrjq0k8jh",
        text: [
          "apple",
          "'s",
          "iphone",
          "8",
          "already",
          "outselling",
          "samsung",
          "'s",
          "new",
          "galaxy",
          "s9",
          "s9",
          "visit"
        ],
        label: "neg"
      },
      {
        raw_text: "Tired of your old phone ..... come see how i can change your life with 50% off A Samsung S9🙊👀Come See me Ericka at 2145 Union St !!!!! I'm Here till 8pm 💋 https://t.co/AyU63Vz1Zp",
        text: [
          "tired",
          "old",
          "phone",
          "come",
          "see",
          "change",
          "life",
          "50",
          "samsung",
          "s9",
          "come",
          "see",
          "ericka",
          "2145",
          "union",
          "st",
          "'m",
          "till",
          "8pm"
        ],
        label: "neg"
      },
      {
        raw_text: "Tryna keep the sun out of my eyes on this beautiful Saturday in St. Philip's, really heating up here at the Regatta. Pics courtesy of East Com's Samsung Galaxy S9 and the Coast Cruiser from Terra Nova GMC Buick https://t.co/BiK9kD4eMa",
        text: [
          "tryna",
          "keep",
          "sun",
          "eyes",
          "beautiful",
          "saturday",
          "st",
          "philip",
          "'s",
          "really",
          "heating",
          "regatta",
          "pics",
          "courtesy",
          "east",
          "com",
          "'s",
          "samsung",
          "galaxy",
          "s9",
          "coast",
          "cruiser",
          "terra",
          "nova",
          "gmc",
          "buick"
        ],
        label: "neg"
      },
      {
        raw_text: "A truly amazing opportunity for doctors is here, check it #DidYouKnowDoc lucky winner will also get Samsung S9 https://t.co/E1VgjFzqAf",
        text: [
          "truly",
          "amazing",
          "opportunity",
          "doctors",
          "check",
          "lucky",
          "winner",
          "also",
          "get",
          "samsung",
          "s9"
        ],
        label: "pos"
      },
      {
        raw_text: "Samsung Galaxy S9 For Rs 7,990: Here Are The Details.. https://t.co/ciU4s8FiRP",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "rs",
          "7",
          "990",
          "details"
        ],
        label: "pos"
      },
      {
        raw_text: "Hello guys🖐️ Today I have come up with a nice product for you. 2018 Newest #Bluetooth #Smart #Watch #Touchscreen with #Camera,#Unlocked Watch #Phone with #Sim Card Slot. #STAINLESS #steel #surface. Multi-Function Smart Watch. So friend get it.. Here is: https://t.co/RfZaHMgKl1 https://t.co/jsETPx8S06",
        text: [
          "hello",
          "guys",
          "today",
          "come",
          "nice",
          "product",
          "2018",
          "newest",
          "watch",
          "card",
          "slot",
          "multi",
          "function",
          "smart",
          "watch",
          "friend",
          "get"
        ],
        label: "pos"
      },
      {
        raw_text: "S9 out here doin backflips wit the camera #samsung https://t.co/BTAP5rumil",
        text: [
          "s9",
          "doin",
          "backflips",
          "wit",
          "camera"
        ],
        label: "pos"
      },
      {
        raw_text: "RT @vzw786wireless: Free Memory Card + Save up to $300 when you buy Samsung S9/S9+ Trade in required. New line required. See store for more…",
        text: [
          "free",
          "memory",
          "card",
          "save",
          "300",
          "buy",
          "samsung",
          "s9",
          "s9",
          "trade",
          "required",
          "new",
          "line",
          "required",
          "see",
          "store"
        ],
        label: "pos"
      },
      {
        raw_text: "Free Memory Card + Save up to $300 when you buy Samsung S9/S9+ Trade in required. New line required. See store for more details. Nearest location here https://t.co/vkcUIZvLRC Rush now https://t.co/TKy7MfJVQK",
        text: [
          "free",
          "memory",
          "card",
          "save",
          "300",
          "buy",
          "samsung",
          "s9",
          "s9",
          "trade",
          "required",
          "new",
          "line",
          "required",
          "see",
          "store",
          "details",
          "nearest",
          "location",
          "rush"
        ],
        label: "pos"
      },
      {
        raw_text: "Samsung Could Launch Three Galaxy S10 Phones In 2019, Here's What We Know So Far As we reported earlier this morning, Galaxy S9 sales haven't exactly been through the roof for Samsung. While the Galaxy S9 remains a strong seller in the Android smartphon… https://t.co/zuP3IUMRje",
        text: [
          "samsung",
          "could",
          "launch",
          "three",
          "galaxy",
          "s10",
          "phones",
          "2019",
          "'s",
          "know",
          "far",
          "reported",
          "earlier",
          "morning",
          "galaxy",
          "s9",
          "sales",
          "exactly",
          "roof",
          "samsung",
          "galaxy",
          "s9",
          "remains",
          "strong",
          "seller",
          "android",
          "smartphon"
        ],
        label: "neg"
      },
      {
        raw_text: "The Samsung Galaxy S9 is here!Come to Sprint and you can save half off with our Sprint Flex Lease. 18853 Ventura Blvd Tarzana (818)344-91356 https://t.co/ceZb3pRfsV",
        text: [
          "samsung",
          "galaxy",
          "s9",
          "come",
          "sprint",
          "save",
          "half",
          "sprint",
          "flex",
          "lease",
          "18853",
          "ventura",
          "blvd",
          "tarzana",
          "818",
          "344",
          "91356"
        ],
        label: "neg"
      },
      {
        raw_text: "RT @Mobilescouk: The brand new #TitaniumGrey edition of the #SamsungGalaxyS9 and #SamsungGalaxyS9Plus is available to order now. Get yours…",
        text: [
          "brand",
          "new",
          "edition",
          "available",
          "order",
          "get"
        ],
        label: "pos"
      },
      {
        raw_text: "@swamie07 I can see how a missing feature can be concerning! The manufacturer releases these updates, so you can contact them directly and leave feedback here: https://t.co/XneVfeE3om Moreover, you can stay up to date with the latest releases here: https://t.co/iNXIuVj1jT ^ALM",
        text: [
          "see",
          "missing",
          "feature",
          "concerning",
          "manufacturer",
          "releases",
          "updates",
          "contact",
          "directly",
          "leave",
          "feedback",
          "moreover",
          "stay",
          "date",
          "latest",
          "releases",
          "alm"
        ],
        label: "pos"
      },
      {
        raw_text: "⭐ 6 best features of Samsung Galaxy S9 ⭐ Read more HERE ▶️ https://t.co/jstsjx1oqN ◀ https://t.co/IVeSFM9RqV",
        text: [
          "6",
          "best",
          "features",
          "samsung",
          "galaxy",
          "s9",
          "read"
        ],
        label: "pos"
      },
      {
        raw_text: "⭐ Samsung sees a second-quarter slump, due in part to slow Galaxy S9 sales ⭐ Read more HERE ▶️ https://t.co/SplIS1t7vQ ◀ https://t.co/h2yAJCEfOz",
        text: [
          "samsung",
          "sees",
          "second",
          "quarter",
          "slump",
          "due",
          "part",
          "slow",
          "galaxy",
          "s9",
          "sales",
          "read"
        ],
        label: "neg"
      },
      {
        raw_text: "@HEHATECLIFF We have an awesome Samsung Galaxy BOGO going on right now that requires adding a new line! Check out the hot details here: https://t.co/hRMaGkGSuf. *JamieK",
        text: [
          "awesome",
          "samsung",
          "galaxy",
          "bogo",
          "going",
          "right",
          "requires",
          "adding",
          "new",
          "line",
          "check",
          "hot",
          "details",
          "jamiek"
        ],
        label: "pos"
      },
      {
        raw_text: "⭐ Galaxy S9 sales may be low, but don't worry, Samsung isn't doomed ⭐ Read more HERE ▶️ https://t.co/goVzomJkFc ◀ https://t.co/Lf2lfuIXLM",
        text: [
          "galaxy",
          "s9",
          "sales",
          "may",
          "low",
          "worry",
          "samsung",
          "doomed",
          "read"
        ],
        label: "neg"
      }
    ],
    positive: 0.59,
    negative: 0.41,
    tweets_count: 178,
    keyword: "\"\"samsung s9\"\"",
    queries_done: 2
  }

export const xiaomi =  {raw_tweets: ["Another blast from the past! Here's another beautiful classic Xiaomi phone. To this date , many Mi Fans consider it one of the best phones ever , the perfect blend of form , beauty and function. Can anyone identify which phone this is? \ud83d\ude0e #Xiaomi #8YearsWithMi #MondayMotivation https://t.co/BsfsokVVD0", "Can England make it to the semi-finals or will Sweden prove too strong? Bob is here to share his wisdom...#ENGSWE #RaiseTheCup https://t.co/uGAPGNnV0O", "Xiaomi , the world's 4th largest smartphone maker , has officially gone public on the Hong Kong Exchange. Here's how the tech titan came to be called \"China's phoenix.\" https://t.co/6PNuP9uNCx https://t.co/XotmmJzA4b", "RT @AdamMaina_: Under half an hour to go. @Xiaomi_KE phones going at massive discounts from just Ksh 8 , 999/= All here: https://t.co/5lJRU\u2026", "RT @gadgetstouse: New Video: https://t.co/ObWZkbzhFr Topic: Interview with Manu Kumar Jain , Global VP at @xiaomi & Managing Director For @\u2026", "@james_simo @MayanMisfit @RealSexyCyborg @GearVita @xiaomi @Ninebot_Inc I permit it currently - so see the images , but can see an isue here in that 'sensitive' is a very course grained tool . Though I permit , I've seen little offensive stuff (which I wouldn't see -- and Naomi's tweets aren't at all). Tricky to get right though in SM", "RT @uptin: Is Xiaomi IPO considered a failure? - @tculpan \u201cIf anyone in here can start a $54B company in 7 or 8 years , let me know\u201d - @ann\u2026", "RT @tech2eets: Here's your easy guide to the best offers during #AmazonPrimeDay. Also , if you are not a Prime member , read how you can get\u2026", "RT @tech2eets: Here's your easy guide to the best offers during #AmazonPrimeDay. Also , if you are not a Prime member , read how you can get\u2026", "RT @tech2eets: Here's your easy guide to the best offers during #AmazonPrimeDay. Also , if you are not a Prime member , read how you can get\u2026", "Here's your easy guide to the best offers during #AmazonPrimeDay. Also , if you are not a Prime member , read how you can get on board now. https://t.co/9MJCt02Vbm @amazonIN #PrimeDay #PrimeDay2018", "RT @donovansung: Another blast from the past! Here's another beautiful classic Xiaomi phone. To this date , many Mi Fans consider it one of\u2026", "@james_simo @RealSexyCyborg @GearVita @xiaomi @Ninebot_Inc They're all showing fine , here? Maybe a geo thing?", "@statanalyst I see you have a Xiaomi Phone Same here \ud83d\ude42", "selling my new sealed Xiaomi Redmi 5 (3/32) in gold color here: https://t.co/P7HmaJOZli", "@OneAssist_Cares @oneassist_in @xiaomi Another 48 hrs..I am not getting any solution to my case but mails and mails to wait for next 48 hrs. Here's another proof. https://t.co/EoTLwsPQmr", "RT @donovansung: Another blast from the past! Here's another beautiful classic Xiaomi phone. To this date , many Mi Fans consider it one of\u2026", "RT @xiaomi_ke: Jumia Anniversary is LIVE & LOUD! Here's XIAOMI portfolio. The choice is yours! https://t.co/Gr3MSw9lbT #JumiaAt6 #JumiaAnni\u2026", "RT @xiaomi_ke: Jumia Anniversary is LIVE & LOUD! Here's XIAOMI portfolio. The choice is yours! https://t.co/Gr3MSw9lbT #JumiaAt6 #JumiaAnni\u2026", "RT @xiaomi_ke: Jumia Anniversary is LIVE & LOUD! Here's XIAOMI portfolio. The choice is yours! https://t.co/Gr3MSw9lbT #JumiaAt6 #JumiaAnni\u2026", "RT @xiaomi_ke: Jumia Anniversary is LIVE & LOUD! Here's XIAOMI portfolio. The choice is yours! https://t.co/Gr3MSw9lbT #JumiaAt6 #JumiaAnni\u2026", "Jumia Anniversary is LIVE & LOUD! Here's XIAOMI portfolio. The choice is yours! https://t.co/Gr3MSw9lbT #JumiaAt6 #JumiaAnniversaryKe https://t.co/d8MbfaVOCR", "@xiaomi Wow , new Android One Device from Xiaomi , to Make Fun of ANDROID ONE..here you can't provide stable update for miA1 and Xiaomi launching MiA2 really funny\ud83d\ude02\ud83d\ude02 \ud83d\udca9\ud83d\udca9\ud83d\udca9#shittyMIA1 #Xiaomifails", "RT @donovansung: Another blast from the past! Here's another beautiful classic Xiaomi phone. To this date , many Mi Fans consider it one of\u2026", "Share theme: Full white - Here's a theme I thought you might like: Full white! Link: https://t.co/GFsu7aZgKo", "XIAOMI MI 6X HATSUNE MIKU SPECIAL EDITION look what you did here...", "RT @donovansung: Another blast from the past! Here's another beautiful classic Xiaomi phone. To this date , many Mi Fans consider it one of\u2026", "Like and Share if you want this USB Cable USB-A Micro USB Suntaiho Nylon Braided For Samsung Xiaomi LG Huawei Meizu Tag a friend who would love this! FREE Shipping Worldwide Buy one here---> https://t.co/r4A3vWXcrc https://t.co/0NoMvcTJJh", "Another blast from the past! Here's another beautiful classic Xiaomi phone. To this date , many Mi Fans consider it one of the best phones ever , the perfect blend of form , beauty and function. Can anyone identify which phone this is? \ud83d\ude0e #Xiaomi #8YearsWithMi #MondayMotivation https://t.co/BsfsokVVD0", "RT @gadgetstouse: New Video: https://t.co/ObWZkbzhFr Topic: Interview with Manu Kumar Jain , Global VP at @xiaomi & Managing Director For @\u2026", "@miuiandroid I can't register to the forum https://t.co/FnAMiP2Wsm , it said \"Your account has been rejected for the following reason: Your registration has been rejected as it resembles spam-like or automated behavior.\" need help here...", "RT @gadgetstouse: New Video: https://t.co/ObWZkbzhFr Topic: Interview with Manu Kumar Jain , Global VP at @xiaomi & Managing Director For @\u2026", "RT @AdamMaina_: Under half an hour to go. @Xiaomi_KE phones going at massive discounts from just Ksh 8 , 999/= All here: https://t.co/5lJRU\u2026", "RT @AdamMaina_: Under half an hour to go. @Xiaomi_KE phones going at massive discounts from just Ksh 8 , 999/= All here: https://t.co/5lJRU\u2026", "RT @AdamMaina_: Under half an hour to go. @Xiaomi_KE phones going at massive discounts from just Ksh 8 , 999/= All here: https://t.co/5lJRU\u2026", "Under half an hour to go. @Xiaomi_KE phones going at massive discounts from just Ksh 8 , 999/= All here: https://t.co/5lJRUoOxmu #JumiaAnniversaryKe #JumiaAt6 https://t.co/QjyFiC1vkN", "Order here\u27a4 https://t.co/b76JltKU5D Best Prices Xiaomi Mijia Upgrade Version Smart Home WiFi Remote Control Multifunctional Gateway Work with Smart Sensor Kit \u27a4 Best Price\u27a4 USD Price 57.77. https://t.co/3aTrXX4Jlv", "Xiaomi Redmi 5 Plus 4GB RAM 64GB ROM 4000mAh Get it here ---> https://t.co/7Du6ZHq70F #vlueshop https://t.co/b7ovL34ndO", "RT @gadgetstouse: New Video: https://t.co/ObWZkbzhFr Topic: Interview with Manu Kumar Jain , Global VP at @xiaomi & Managing Director For @\u2026", "RT @JumiaGhana: Live at 12 midnight. Get ready to grab the Xiaomi Redmi 5A selling for only GH\u00a2 345 only on the Jumia App Download the APP\u2026", "RT @JumiaGhana: Live at 12 midnight. Get ready to grab the Xiaomi Redmi 5A selling for only GH\u00a2 345 only on the Jumia App Download the APP\u2026", "Live at 12 midnight. Get ready to grab the Xiaomi Redmi 5A selling for only GH\u00a2 345 only on the Jumia App Download the APP here>> https://t.co/UK1pHg5yr2 and be ready! https://t.co/JD7Wy1EMec", "@Paulo_3005Car @trnrtips Not sure why , but no problems here in a cheaper device still - Xiaomi Mi Max 1st gen", "Only 5 minutes to install and you have a wireless/bluetooth #LED , remote operating light \ud83d\ude0e switch from warm to cool tones , this stylish futuristic light instalment is one to add into your home \u2b50\ufe0f SHOP HERE: https://t.co/oLUad8hRuL https://t.co/QODBrWyCBk", "Order here\u27a4 https://t.co/UmGHojy7Pw Best Prices Xiaomi Body Scale Protective Case Gray \u27a4 Best Price\u27a4 USD Price 21.77. https://t.co/yMwYM1aZoh", "@xiaomi Am writing this with one of those mi3's out here in the world", "@pratik_mishra @XiaomiIndia @RedmiIndia @xiaomi Strange!! Leave their feedback app , they don't even reply here!! Next time , it's pixel!! \ud83d\ude02", "USB Car Charger 4 USB-A Ports AUKEY For Huawei Xiaomi iPhone 8 7 6s Plus iPad Samsung Galaxy Note8 S8 Tag a friend who would love this! FREE Shipping Worldwide Get it here ---> https://t.co/i6LuJeCpBx https://t.co/1w4sUsZBTo", "Like and Share if you want this Smart Car Charger 2 USB DUAL Ports Lighter Adapter Mobile Phone Charger For Xiaomi Tablet iPhone Huawei P10 Samsung Galaxy S8 Tag a friend who would love this! FREE Shipping Worldwide Buy one here---> https://t.co/UpeKOwQFYw https://t.co/NyULI9Mci1", "Mi Phone Dealers in Asansol \u2022 Insat King \u2022 Gayatri Tele World \u2022 Sree Lakshmi Enterprise \u2022 Narayan Enterprises are selling Redmi phones with an extra charge of upto \u20b9500 which is straight up black marketing of Xiaomi devices. Any fix? @MiIndiaSupport @XiaomiIndia @RedmiIndia", "Here's everything you need to know about the #XiaomiMiMax3 ahead of launch https://t.co/PsEtcboBwT", "Xiaomi Mi Max 3 retail box leaked ahead of launch: Here are the specs https://t.co/RM7QmOZNxt https://t.co/h1rz8wDBj5", "@manukumarjain @xiaomi @XiaomiIndia Left to right Redmi 1S , Redmi 1S , Redmi Note 3 , Redmi Note 4 Lake Blue. Not pictured here , a Redmi 3S. Shot on & Tweeting with a Redmi N4...\ud83d\ude00 https://t.co/eSsXdVT9X3", "@xiaomi_france @xiaomi @donovansung @XiangW_ @manukumarjain @leijun Now that you're here i have a question I broke my redmi note 5 screen , where can i get it repaired ?", "@xiaomi My first Mi phone , managed to grab it in the first sale here in India \u2764\ufe0f", "Mi fans , we're excited to announce our official Mi Fan Meet in Bangladesh! The event will be held on July 17th at 5 PM in Dhaka. Limited seats available; so be sure to register now! Register here: https://t.co/fBjDquQzi1 #MiCommunity #FanMeetUp #MiBangladesh #Xiaomi #Mi https://t.co/NfV3Bp38sU", "Order here\u27a4 https://t.co/psqzelgBPl Best Prices Xiaomi Body Scale Protective Case Gray \u27a4 Best Price\u27a4 USD Price 21.77. https://t.co/cHBhGKJwfr", "@amitbhawani @phoneradarblog @xiaomi Another video of interesting products is here", "Here's a Xiaomi Redmi Note 4 for sale #Xiaomi #RedmiNote4 https://t.co/0WPHQ0sNwn https://t.co/MFALZEDtL4", "XIAOMI Mi Band 2 Miband 2 Smart Bracelet Wristband Band Fitness Tracker Bracelet Smartband Heart rate Monitor 100% Original FREE Shipping Worldwide Get It here ---> https://t.co/GhbSS5dWUZ https://t.co/If91UNZvqm", "RT @tictoc: Here's the TicToc on @technology: -Xiaomi drops on debut as investors question internet vision https://t.co/0tRWu5sTDv -Samsung\u2026", "@xiaomi i need a CDMA compatible smartphone please , its been hell to find one here in Southern Africa coz ee mainly use WCDMA", "#SamsungGalaxyJ8 Vs #XiaomiRedmiNote5Pro Vs #MotorolaMotoE5Plus Find detailed comparison here: https://t.co/FkNrdD6rPZ https://t.co/iPEgoxamQQ", "@TheUnrealMama @netcitizen that's not the point here.. also tell me who doesn't overcharge for accessories and repairs except xiaomi..", "@ssabari61 @xiaomi Exactly , same here. Using this primarily for camera purposes. This phone has one of the best cameras even today! Using a Redmi device for primary as it supports 4G and VoLTE. Otherwise , wouldn't leave my #Mi3 :)", "RT @tictoc: Here's the TicToc on @technology: -Xiaomi drops on debut as investors question internet vision https://t.co/0tRWu5sTDv -Samsung\u2026", "RT @gadgetstouse: New Video: https://t.co/ObWZkbzhFr Topic: Interview with Manu Kumar Jain , Global VP at @xiaomi & Managing Director For @\u2026", "Kicking off Sunday morning with this beautiful product photo of one of our favourite Xiaomi phones. Can anyone identify which phone it is? \ud83e\udd14 #Xiaomi #Sunday - Check out the answer here! \u25b6\ufe0f https://t.co/stkzYmQfkB https://t.co/m4NYpgASoO", "Xiaomi Mi Band 2 Pulse Smart Sport Sleep Heart Rate Monitor Bracelet Fitness Tracker Wristband IP67 Waterproof Strap FREE Shipping Worldwide Get It here ---> https://t.co/wxdFrUCTX0 https://t.co/nK1QisSmdL", "RT @abhishek: New Video: https://t.co/syOfyATwLF Topic: Interview With Manu Kumar Jain ( @manukumarjain ) Mr. Manu Kumar Jain - is Global\u2026", "@xiaomi To continue with expanding since 8ts now here in Africa and also bring more fantastic devices at affordable prices as its been doing since inception", "@shadlenoirdmb @xiaomi The price is kinda the same , but here we earn in \"soles\" not euros or dollars (1US$=3.28 soles , 1\u20ac=3.8 soles) I bought the Redmi Note 5 global , but still , I had all my photos and a SD of 32GB in my Mi A1 :'(", "Rated as the number 1 most interested phone on the internet , you can now buy the #RedmiNote5Pro here! Lowest price guaranteed. \ud83d\ude0e SHOP HERE: https://t.co/r9QtF9Vr2l https://t.co/GRQf3n6kCg", "ROCK Original Micro USB Cable for Samsung/Xiaomi/Meizu/HTC/Huawei , Fast Charging USB Data Cables for Android Mobile Phone Cable FREE Shipping Worldwide Get it here ---> https://t.co/wrEpfB6m8K https://t.co/l3dwCqPs7n", "@phoneradarblog @xiaomi @oppo @Android @Lamborghini @Flipkart @MEIZU @Nokiamobile @amazonIN @Snapchat @Moto @instagram @oneplus I always prefer a good battery good screen in smartphone but......... In Mi Max 3 every thing is here eagerly waiting for Mi Max 3", "USB Cable USB-A USB-C 3 Pcs BlitzWolf 1m 1.8m 2.5m For Huawei Xiaomi Mac Set Tag a friend who would love this! FREE Shipping Worldwide Get it here ---> https://t.co/5XDWX9hend https://t.co/wAdPGbfp9y", "@xiaomi Worst experience ever and worst software support for Android One device like Mi A1. Every single Android one devices upgraded to 8.1 here is Mi A1 which is still on 8.1 \ud83d\ude02\ud83d\ude02 improve your software support for Android one devices.", "@xiaomi There are some roberrs here in #Kanpur #MIServiceCenter They Demand me for preparing my cell phone 9860 INR . Only Some Water was inside Which I Got Prepared From Outside Only Rs 700 . So Request to @MiIndiaSupport Give them a Gun So That They Can Robb Us Properly", "Here are few upcoming Xiaomi devices spotted on EEC for European markets. #TechNews @XiaomiIndia @xiaomi https://t.co/0SqRoNz4Rk", "@rajsaurabh_007 @manukumarjain Don't forget that samsung is setting up mfg plant only to make in india for the world market , but #Xiaomi and #honor will be here for making premium smartphones in reasonable price to reach every indian", "@phoneradarblog @xiaomi @XiaomiIndia Much Awaited Smartphone is Here...\ud83d\udc4d\ud83d\udc4d", "Xiaomi - Executive - Warehouse Operations (3-4 yrs) Anywhere in India/Multiple Locations (Logistics / Warehousing / Distribution).. Read More here.....", "Xiaomi - Executive - Operations (3-4 yrs) Anywhere in India/Multiple Locations (Logistics / Warehousing / Distribution).. Read More here.....", "@XiangW_ @xiaomi Kindly come to Nigeria. The most populous and most vibrant economy in Africa. Do what u did in India here , I bet you , you've opened the doors to the whole of Africa. Please come NOW", "RT @AliExpress_EN: We all have that #friend\ud83d\udc6c\ud83d\udc6d who is a bit too OCD when it comes to #hygiene\ud83d\ude0d\ud83d\ude00. So here's the perfect #gift \ud83c\udf81for them! A co\u2026", "USB Wall Charger USB-A Quick Charger 3.0 Ugreen For Samsung Xiaomi Tag a friend who would love this! FREE Shipping Worldwide Get it here ---> https://t.co/HWaHyrI8qc https://t.co/mRVknrC477", "New video is here unboxing of redmi note 5 pro Link:https://t.co/nXX3HoRVyx #Xiaomi #RedmiNote5Pro #MI https://t.co/0ynNODbu9O", "New video is here unboxing of redmi note 5 pro Link:https://t.co/nXX3HoRVyx #Xiaomi #RedmiNote5Pro #MI https://t.co/QTK5yx36cV", "Case For Xiaomi Redmi 4A Luxury Wallet PU Leather Case Stand Flip Card Hold Phone Cover Bags For Xiaomi Redmi 4A FREE Shipping Worldwide Get it here ---> https://t.co/gCQRBh8HnM https://t.co/Z2SSoVQZns", "@xiaomi Instagrm ?? Why not here Thats bad rule", "RT @abhishek: New Video: https://t.co/syOfyATwLF Topic: Interview With Manu Kumar Jain ( @manukumarjain ) Mr. Manu Kumar Jain - is Global\u2026", "Xiaomi Mi Crowdfunding Launched in India: Here's How it Works https://t.co/dL1STf2qBP https://t.co/xCXCQGrBvu", "#Xiaomi IPO: Here's what you need to know about the Chinese smartphone maker's public debut https://t.co/S1sgyVUQsy", "1M LED Light Micro USB Cable Adapter Charger Data Sync Cord For Samsung Hauwei Xiaomi Android Phone Tablet USB Charging Cable FREE Shipping Worldwide Get it here ---> https://t.co/ipvS6cWIFy https://t.co/U8cdIGwS0M", "RT @wickedstepmami: @Ifeagbeja There are so many affordable and strong phones in that range , check xiaomi , oukitel , Huawei , ulefone. I don'\u2026", "Like and Share if you want this Dragon Ball Z phone cases Tag a friend who would love this! FREE Shipping Worldwide Get it here ---> https://t.co/kVo6GgVd3X https://t.co/UAIptmkR2c", "RT @gadgetstouse: New Video: https://t.co/ObWZkbzhFr Topic: Interview with Manu Kumar Jain , Global VP at @xiaomi & Managing Director For @\u2026", "Mi Fans Kenya , Continue to share their thoughts on our new AI CAMERA BEAST! #RedmiNote5 learn more about it here; https://t.co/Gr3MSw9lbT @JumiaKenya https://t.co/WzxtSMr08m", "Here's a theme I thought you might like: Gomu Gomuno! Link: https://t.co/xweVhibovS", "RT @amitbhawani: SJCAM SJ8 Pro - Better Action Camera than Xiaomi , Cheaper than GoPro? Watch Video - https://t.co/MBPjgemAqk \ud83d\udcf7 Buy this Ca\u2026", "@XiaomiIndia , @xiaomi , Regarding to gurgaon sector 14 service center QDG service limited. Here very poor service center , including first check up , and repairmen. They are Consuming half day only for checking the phone problem. Very unresponsive people.", "@xiaomi Every product that launch in China should equally launch here", "Back to the xiaomi mi a1. I do not want to talk about the spec here but musical ability", "Here are the top 5 smartphones in India: Yes , Xiaomi Redmi 5A is no surprise , but the other names might be - The Indian Express https://t.co/rfswzhBp6y https://t.co/3mLxc4mvsZ", "Here are the top 5 smartphones in India: Yes , Xiaomi Redmi 5A is no surprise , but the other names might be - The Indian Express https://t.co/lON8EkLq5x", "Here are the top 5 smartphones in India: Yes , Xiaomi Redmi 5A is no surprise , but the other names might be - The Indian Express https://t.co/7ABnkjT3PT", "Here is the right screenshot @XiaomiIndia @manukumarjain @XiaomiIndia @xiaomi #MadeForTheMove https://t.co/n59CfZJnOW", "@manukumarjain @xiaomi @RedmiIndia @MiIndiaSupport @Miindia #1SmartphoneBrand in India should also have #1SmartphoneService. Customer service centre here in Panjim-Goa was shut down since 10.30 am. I waited outside the centre from 10.15 am to 12 pm. #MIstake buying MI Melvyn Goa", "RT @FoneArena: Xiaomi appoints former Jabong executive Muralikrishnan B as India COO https://t.co/naYilxnt58 https://t.co/VZFm8tuXKP", "RT @xiaomi: #RaiseTheCup to do good beyond the game. 1. Send us a pic of you raising a cup. 2. Add #RaiseTheCup and #Xiaomi. 3. Tag 3 frien\u2026", "https://t.co/9RdAPXZIRg Alert for Mi micromax vivo etc low cost mobile users !! You privecy at the mercy of hackers!! My long time fear has come true with the various chinese brands or co-brands of mobile. The threat is much higher then what is visible now or reported here.", "USB Cable USB-A Micro USB Vention 1m 2m 3m For Samsung Xiaomi LG Huawei Tag a friend who would love this! FREE Shipping Worldwide Buy one here---> https://t.co/sFSjk3RJsv https://t.co/NyIs313glZ", "\ud83d\udecd\ufe0f Super Sale up to 80% for a limited time only.\ud83d\uded2 Like and Share if you want this \u261d\ufe0f\u261d\ufe0f\u261d\ufe0f for $280.00. \ud83d\udc4c \ud83e\udd11 Apply \ud83c\udfab code: HSSMDS30\ud83c\udff7\ufe0f to get additional 30% Off.\ud83d\udd16 Buy one here \ud83d\udc49 https://t.co/vg0XxRrQKJ https://t.co/VN2GFkB7rQ", "@shantam31 @xiaomi @C4ETech Same here", "Here are the top 5 smartphones in India: Yes , Xiaomi Redmi 5A is no surprise , but the other names might be - The Indian Express https://t.co/q3NlWOz07Z https://t.co/DeWnmaDb6o", "RT @xiaomi: #RaiseTheCup to do good beyond the game. 1. Send us a pic of you raising a cup. 2. Add #RaiseTheCup and #Xiaomi. 3. Tag 3 frien\u2026", "@xiaomi I just want everything you produce to be made available in India! You know that you have a huge fan base here....", "Here are the top 5 smartphones in India: Yes , Xiaomi Redmi 5A is no surprise , but the other names might be https://t.co/hEVqI1t6vV https://t.co/KH1gpnbEUZ", "Here are the top 5 smartphones in India: Yes , Xiaomi Redmi 5A is no surprise , but the other names might b.. https://t.co/xeOM3muiRp", "@u_w_i_n @tldtoday @xiaomi Ik about mi mix but thats honestly not a big brand company here in the U.S.", "Car Phone Holder Air Vent FLOVEME For iPhone 7 7 Plus Samsung S8 Huawei Xiaomi Tag a friend who would love this! FREE Shipping Worldwide Buy one here---> https://t.co/ZT8O3MHK8M https://t.co/FFQpBsUoZ5", "@xiaomi @donovansung @manukumarjain @XiangW_ @leijun Xiaomi Redmi S2 here from The Philippines!! \ud83d\ude18\ud83d\ude18\ud83d\ude18", "@xiaomi @donovansung @manukumarjain @XiangW_ @leijun I don't have any now but am dying to have one called redmi 6 pro. But my location is here in Nigeria.", "@C4ETech @xiaomi @manukumarjain @jaimani Same here. The \"big boys\" over here in the US were late to the game regarding @xiaomi but I was already in the know. I haven't got to buy my first phone from them due to restricted finances but they are the top of my list.", "RT @timesofindia: Xiaomi launches a new gaming mouse , Yu Y720 Lite Details here: https://t.co/ZKLhY4H4sl via @gadgetsnow https://t.co/grF\u2026", "#Xiaomi #MiA2 Camera sample is here. (12MP+20MP Rear camera setup) https://t.co/g9UBmAEJFw", "RT @jrharbort: It's so beautiful , can't wait until it gets here! #xiaomi #\u521d\u97f3\u30df\u30af #\u521d\u97f3\u672a\u4f86 #\u5c0f\u7c73 https://t.co/f9xYINd78S", "#GalaxyOn6 #WorldCup #URUFRA #HappyBirthdayMsDhoni check when ur #Xiaomi smartphone will get MIUI 10 update https://t.co/mnFvbJ7tiJ check it out here #YouTube #YouTubers #Bologuruji https://t.co/eNz5BbhMd8", "Here is my new Video on my #YouTube Channel Best #xiaomi_smartphones under 15000 rupees 2018!! #Trending_Techy https://t.co/fOeffxMXgk", "Xiaomi Laptop Notebook Air HD 12.5 inch Intel CoreM-7Y30 Dual Core 4GB RAM 128GB SSD for Windows10 with M3 Processor for just $1607.04. Order here https://t.co/YNyzU7AXP7 https://t.co/PoSRgZdXmn", "RT @YourStoryCo: Shifting gears from being #Jabong's Co-founder to heading @XiaomiIndia , here's @manukumarjain's journey - @xiaomi #Xiaom\u2026", "RT @phoneradarblog: Here is the official first look of the Xiaomi Black Shark Gaming Smartphone Stay tuned: https://t.co/PYG6avJTvH @Xiaom\u2026", "USB Cable USB-A Micro USB USB-C Lightning NOHON For iPhone 7 6 6S Plus Samsung Xiaomi LG Tag a friend who would love this! FREE Shipping Worldwide Buy one here---> https://t.co/Ps6flbYJFn https://t.co/TyYDZJmv2n", "RT @tictoc: Here's what's coming up on #tictocnews - U.S. hiring in June tops forecast - Supreme Court pick could test Mueller - Xiaomi sha\u2026", "Here's what's coming up on #tictocnews - U.S. hiring in June tops forecast - Supreme Court pick could test Mueller - Xiaomi shares officially start trading https://t.co/jzVmk5e1IJ", "RT @BryanKinuthia: @Miss_Klowee @xiaomi_ke @JumiaKenya If they don't respond here publicly , I sure will", "@Miss_Klowee @xiaomi_ke @JumiaKenya If they don't respond here publicly , I sure will", "First #XiaomiMiA2 #camera sample is here https://t.co/0K9YEOwHMY https://t.co/Sd9eIET7I1", "RT @beebomco: Here's the review you've been waiting for. #Xiaomi #BlackShark https://t.co/ulYJTdDXUH", "@Murtaza_Rizvi @xiaomi @XiaomiIndia I guess its has been 2 months now since service center was opened here", "@Murtaza_Rizvi @xiaomi @XiaomiIndia There service centre is already here at Qamarwari", "RT @PeopleMatters2: .@XiaomiIndia's Global Vice President and MD , @manukumarjain announced on Twitter the appointment of their new COO. Her\u2026", "Order here\u27a4 https://t.co/H4BCHzFH0t Best Prices Xiaomi Miband Water Drop Shaped Necklace Pendant Soft Silicone Case Green \u27a4 Best Price\u27a4 USD Price 3.77. https://t.co/CYqcfgJ9Dn", "Read it here first- @XiaomiIndia appoints ex @myntra head as #COO. @hawkeye @manukumarjain @PeopleMatters2 https://t.co/qQNAKKpoyi @Ester_Matters", "The real story here is Xiaomi and Huawei's continued rapid growth. Brands like HTC and LG missed out by continuing to try to go toe to toe with Apple and Samsung with their price points. https://t.co/JbVx370SFe", "RT @rohit_khurana: Xiaomi Mi 6X (Mi A2) Gets Leaked Again , Reveals Software Features https://t.co/EcXALbCN1K https://t.co/LoI049kmIp", ".@XiaomiIndia's Global Vice President and MD , @manukumarjain announced on Twitter the appointment of their new COO. Here's all that you need to know. https://t.co/Yy2Irc1OrW", "RT @SeanUM_China: Here's my latest @Youtube video on #China's\ud83c\udde8\ud83c\uddf3 @Xiaomi: https://t.co/NFjdHkjCY0 I'm a huge fan of all their #SmartHome\ud83e\udde0 a\u2026", "@smartphonejock @xiaomi @donovansung @manukumarjain @XiangW_ @leijun Mi6 user here too...", "@gadgetstouse \ud83d\ude02\ud83d\ude02 and here I'm still hating Xiaomi for their own ui \ud83d\ude2a\ud83d\ude2a", "@xiaomi My brother the first person to introduce me about MI and now iam having MI A1 Follow by MI 4 , MI 4a\ud83d\ude0d\ud83d\ude0e.!!! Here my brother tagging in @sady89 #8YearsWithMi", "@xiaomi My brother the first person to introduce me about MI and now iam having MI A1 Follow by MI 4 , MI 4a\ud83d\ude0d\ud83d\ude0e.!!! Here my brother tagging in @sady89", "@xiaomi Welcome @xiaomi I'm proud to say I'm Mi fan from India \ud83d\ude0d\u2764\ufe0f #8YearsWithMi #NoMiWithoutYou \ud83d\ude0d\u2764\ufe0f Here is My Journey with Mi \u2764\ufe0f https://t.co/REu1wHJ45H https://t.co/GerWkRV9WP", "RT @jrharbort: It's so beautiful , can't wait until it gets here! #xiaomi #\u521d\u97f3\u30df\u30af #\u521d\u97f3\u672a\u4f86 #\u5c0f\u7c73 https://t.co/f9xYINd78S", "RT @SeanUM_China: Here's my latest @Youtube video on #China's\ud83c\udde8\ud83c\uddf3 @Xiaomi: https://t.co/NFjdHkjCY0 I'm a huge fan of all their #SmartHome\ud83e\udde0 a\u2026", "@xiaomi Its me who got introduced to mi I was searching for a good mobile at my budget and landed here. And u know what I didn't regret after buying it!!! #8YearsWithMi", "@shantam31 @xiaomi @C4ETech same here\u270b\u270b", "@XiangW_ @xiaomi Big fan of Mi but they sell only mid-range items here. I wanted to buy Mi laptop and waited for 6 months now I'm going to buy Asus zenbook.", "$38 Dash Cam That's Not Bad https://t.co/1zk2IR0lXu XIAOMI 70MAI Smart Car DVR EU US Version 1080P 130 Degree Wide Angle IMX323 Sensor Voice Control GET IT HERE!!!! https://t.co/5KGB5yPpd4", "@izaqkull @AB_Brainiac you can thank me here \ud83d\ude0e https://t.co/k0kuMYbIu1"], "products": [{name: "xiaomi", count: 31}, {name: "xiaomi redmi 5a", count: 9}, {name: "indian", count: 5}, {name: "samsung", count: 4}, {name: "huawei", count: 3}, {name: "xiaomi body scale", count: 2}, {name: "lg", count: 2}, {name: "xiaomi ipo", count: 1}, {name: "geo", count: 1}, {name: "xiaomi redmi 5 3 32", count: 1}, {name: "xiaomi mi 6x hatsune miku", count: 1}, {name: "huawei meizu tag", count: 1}, {name: "lg huawei meizu", count: 1}, {name: "samsung xiaomi lg huawei meizu", count: 1}, {name: "xiaomi lg huawei meizu", count: 1}, {name: "xiaomi mijia upgrade", count: 1}, {name: "xiaomi redmi 5", count: 1}, {name: "xiaomi mi max 1st gen", count: 1}, {name: "huawei xiaomi iphone 8 7 6s", count: 1}, {name: "samsung galaxy note8 s8", count: 1}, {name: "xiaomi iphone 8 7 6s", count: 1}, {name: "huawei p10 samsung", count: 1}, {name: "samsung galaxy s8", count: 1}, {name: "xiaomi tablet iphone huawei p10 samsung", count: 1}, {name: "xiaomi mi max 3", count: 1}, {name: "xiaomi redmi note 4", count: 1}, {name: "xiaomi mi band 2 miband 2", count: 1}, {name: "xiaomi mi band 2", count: 1}, {name: "htc huawei", count: 1}, {name: "samsung xiaomi meizu htc huawei", count: 1}, {name: "xiaomi meizu htc huawei", count: 1}, {name: "huawei xiaomi mac", count: 1}, {name: "xiaomi mac set", count: 1}, {name: "xiaomi executive operations 3 4 yrs", count: 1}, {name: "samsung xiaomi tag", count: 1}, {name: "xiaomi redmi 4a", count: 1}, {name: "xiaomi mi crowdfunding", count: 1}, {name: "samsung hauwei xiaomi", count: 1}, {name: "xiaomi android phone", count: 1}, {name: "ca", count: 1}, {name: "gopro watch video", count: 1}, {name: "xiaomi mi a1", count: 1}, {name: "3m", count: 1}, {name: "lg huawei tag", count: 1}, {name: "samsung xiaomi lg huawei", count: 1}, {name: "xiaomi lg huawei", count: 1}, {name: "huawei xiaomi tag", count: 1}, {name: "samsung s8 huawei xiaomi", count: 1}, {name: "xiaomi redmi s2", count: 1}, {name: "intel corem 7y30", count: 1}, {name: "xiaomi black shark", count: 1}, {name: "samsung xiaomi lg", count: 1}, {name: "xiaomi lg tag", count: 1}, {name: "xiaomi sha", count: 1}, {name: "xiaomi miband water", count: 1}, {name: "htc", count: 1}, {name: "xiaomi mi 6x mi a2", count: 1}, {name: "asus zenbook", count: 1}, {name: "xiaomi 70mai", count: 1}], queries_done: 2, tweets_count: 160, keywrod: "\"xiaomi\""}
export const xiaomi1 = {data: [{raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by: Hadlee Simons", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know", "hadlee", "simons"], label: "pos"}, {raw_text: "Here's everything you need to know about the #XiaomiMiMax3 ahead of launch https://t.co/PsEtcboBwT", text: ["'s", "everything", "need", "know", "ahead", "launch"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 retail box leaked ahead of launch: Here are the specs https://t.co/RM7QmOZNxt https://t.co/h1rz8wDBj5", text: ["xiaomi", "mi", "max", "3", "retail", "box", "leaked", "ahead", "launch", "specs"], label: "neg"}, {raw_text: "@phoneradarblog @xiaomi @oppo @Android @Lamborghini @Flipkart @MEIZU @Nokiamobile @amazonIN @Snapchat @Moto @instagram @oneplus I always prefer a good battery good screen in smartphone but......... In Mi Max 3 every thing is here eagerly waiting for Mi Max 3", text: ["always", "prefer", "good", "battery", "good", "screen", "smartphone", "mi", "max", "3", "every", "thing", "eagerly", "waiting", "mi", "max", "3"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/RaejrOL346 #SmartNews", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know: *a rear fingerprint scanner, and Android 8.1. *Mi Max 3 will have a 6.9-inch full HD+ screen (18:9), paired with a massive 5,400mAh battery. https://t.co/XW5JXP5886", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know", "rear", "fingerprint", "scanner", "android", "8", "1", "mi", "max", "3", "6", "9", "inch", "full", "hd", "screen", "18", "9", "paired", "massive", "5", "400mah", "battery"], label: "neg"}, {raw_text: "#Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/OoGJm8J1cD", text: ["mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know - https://t.co/yMbs2MRbJL #android #smartphones", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @91mobiles: #Xiaomi Mi Max 3 confirmed to launch on July 19th: here's what we know so far https://t.co/cazmx9mxnZ https://t.co/mkBrbj8FWd", text: ["mi", "max", "3", "confirmed", "launch", "july", "19th", "'s", "know", "far"], label: "neg"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here&#8217;s what you should know https://t.co/Ybat713qrK", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/fa4Rz3O1KD https://t.co/zRbYALf78D", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/U3BJTAFurz", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @91mobiles: #Xiaomi Mi Max 3 confirmed to launch on July 19th: here's what we know so far https://t.co/cazmx9mxnZ https://t.co/mkBrbj8FWd", text: ["mi", "max", "3", "confirmed", "launch", "july", "19th", "'s", "know", "far"], label: "neg"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/30P58tAmqc", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/DoiGaePHLS https://t.co/MHsari9pTO", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @91mobiles: #Xiaomi Mi Max 3 confirmed to launch on July 19th: here's what we know so far https://t.co/cazmx9mxnZ https://t.co/mkBrbj8FWd", text: ["mi", "max", "3", "confirmed", "launch", "july", "19th", "'s", "know", "far"], label: "neg"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know - https://t.co/gJ5veliMP3", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/5AY2jF7mFp Via AndroidAuthority https://t.co/HnYAhR2syD", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know", "via", "androidauthority"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @91mobiles: #Xiaomi Mi Max 3 confirmed to launch on July 19th: here's what we know so far https://t.co/cazmx9mxnZ https://t.co/mkBrbj8FWd", text: ["mi", "max", "3", "confirmed", "launch", "july", "19th", "'s", "know", "far"], label: "neg"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/rFlKXJcjKj https://t.co/deetAkQ0eo", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "#Xiaomi Mi Max 3 confirmed to launch on July 19th: here's what we know so far https://t.co/cazmx9mxnZ https://t.co/mkBrbj8FWd", text: ["mi", "max", "3", "confirmed", "launch", "july", "19th", "'s", "know", "far"], label: "neg"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/SU8jxH79DT #News #Xiaomi #XiaomiMiMax3", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NrpScrekNX", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "#Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/Jgx0uvIfRb https://t.co/tNOd6MXC1M", text: ["mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know - https://t.co/ADUsVGlFgK https://t.co/e1LyVit5Xd", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/9oMFB9CK5d https://t.co/IxwXVtb7Qk", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know We've got exactly a week to go until Xiaomi reveals its Mi Max 3 phablet, so what should you expect? https://t.co/mUQ13sQxJz https://t.co/D0HstSEW7V", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know", "'ve", "got", "exactly", "week", "go", "xiaomi", "reveals", "mi", "max", "3", "phablet", "expect"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/jysKiBc7a1 https://t.co/88SqG4OccK", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT AndroidAuth : Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NX7PnSmP68 #xiaomi #xiaomimimax3 by: Hadlee Simons https://t.co/yjUbMacxEx | https://t.co/TXqI1QJA2t", text: ["androidauth", "xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know", "hadlee", "simons"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/Js9WBmPDAX https://t.co/0gNcEbPGKy", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @slideme: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/1Cw7O7HAjS @slideme https://t.co/unt9nQ\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/6OTzkn0nL9 #Android", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/Mf0SzSerSw https://t.co/kpItVoXOIC", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/1Cw7O7HAjS @slideme https://t.co/unt9nQ2sXc", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "RT @phoneradarblog: Xiaomi #MiMax3 Confirmed to Launch on July 19th in China Read More: https://t.co/HUpJCMR5cU @xiaomi @XiaomiIndia https\u2026", text: ["xiaomi", "confirmed", "launch", "july", "19th", "china", "read", "https"], label: "neg"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/OiX6Fiyj7k #xiaomi #xiaomimimax3 by: Hadlee Simons", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know", "hadlee", "simons"], label: "pos"}, {raw_text: "RT @AndroidAuth: Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by\u2026", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 reveal confirmed for July 19: Here's what you should know https://t.co/NdzesA6FFG #xiaomi #xiaomimimax3 by: Hadlee Simons", text: ["xiaomi", "mi", "max", "3", "reveal", "confirmed", "july", "19", "'s", "know", "hadlee", "simons"], label: "pos"}, {raw_text: "Xiaomi Mi Max 3 Smartphone Is Going To Launch On July 19 In China: Here Is Everything You Need To Know \u279f https://t.co/gF0W6ZRruo #xiaomi #mi #max3 #mimax3smartphone #tech https://t.co/3Vor71Tq93", text: ["xiaomi", "mi", "max", "3", "smartphone", "going", "launch", "july", "19", "china", "everything", "need", "know"], label: "neg"}, {raw_text: "RT @mobphoneonsale: Original Xiaomi Mi Max 2 4GB 64GB Mobile Phone 6.44\" 1080P Snapdragon 625 Octa Core 12MP 5300mAh QC 3.0 9V 2A Andriod 7\u2026", text: ["original", "xiaomi", "mi", "max", "2", "4gb", "64gb", "mobile", "phone", "6", "44", "1080p", "snapdragon", "625", "octa", "core", "12mp", "5300mah", "qc", "3", "0", "9v", "2a", "andriod", "7"], label: "neg"}, {raw_text: "Original Xiaomi Mi Max 2 4GB 64GB Mobile Phone 6.44\" 1080P Snapdragon 625 Octa Core 12MP 5300mAh QC 3.0 9V 2A Andriod 7.1 OTA FREE Shipping Worldwide Get it here ---> https://t.co/lzkMx6B1lF https://t.co/bSCMVGhJ1t", text: ["original", "xiaomi", "mi", "max", "2", "4gb", "64gb", "mobile", "phone", "6", "44", "1080p", "snapdragon", "625", "octa", "core", "12mp", "5300mah", "qc", "3", "0", "9v", "2a", "andriod", "7", "1", "ota", "free", "shipping", "worldwide", "get"], label: "neg"}], positive: 0.79, negative: 0.21, tweets_count: 52, keyword: "\"\"xiaomi mi max 3\"\"", queries_done: 1}
export const xiaomi2 = {data: [{raw_text: "RT @JumiaGhana: Live at 12 midnight. Get ready to grab the Xiaomi Redmi 5A selling for only GH\u00a2 345 only on the Jumia App Download the APP\u2026", text: ["live", "12", "midnight", "get", "ready", "grab", "xiaomi", "redmi", "5a", "selling", "gh", "345", "jumia", "app", "download", "app"], label: "neg"}, {raw_text: "RT @JumiaGhana: Live at 12 midnight. Get ready to grab the Xiaomi Redmi 5A selling for only GH\u00a2 345 only on the Jumia App Download the APP\u2026", text: ["live", "12", "midnight", "get", "ready", "grab", "xiaomi", "redmi", "5a", "selling", "gh", "345", "jumia", "app", "download", "app"], label: "neg"}, {raw_text: "Live at 12 midnight. Get ready to grab the Xiaomi Redmi 5A selling for only GH\u00a2 345 only on the Jumia App Download the APP here>> https://t.co/UK1pHg5yr2 and be ready! https://t.co/JD7Wy1EMec", text: ["live", "12", "midnight", "get", "ready", "grab", "xiaomi", "redmi", "5a", "selling", "gh", "345", "jumia", "app", "download", "app", "ready"], label: "neg"}, {raw_text: "Xiaomi #Redmi Smartphones are available in store! Get the Redmi 5 (2GB RAM & 16 GB Storage) at Ugx 718,000 or Redmi 5A at only Ugx 538,000 Order here; https://t.co/FVOGnQEdIM https://t.co/PM8MyNe2Ds", text: ["xiaomi", "smartphones", "available", "store", "get", "redmi", "5", "2gb", "ram", "16", "gb", "storage", "ugx", "718", "000", "redmi", "5a", "ugx", "538", "000", "order"], label: "neg"}, {raw_text: "Here are the top 5 #smartphones in India: Yes, Xiaomi Redmi 5A is no surprise, but the other names might be \u2013 The Indian Express https://t.co/a6Mb1fYh5d", text: ["top", "5", "india", "yes", "xiaomi", "redmi", "5a", "surprise", "names", "might", "indian", "express"], label: "pos"}, {raw_text: "Here are the top 5 smartphones in India: Yes, Xiaomi Redmi 5A is no surprise, but the other names might be - The Indian Express https://t.co/rfswzhBp6y https://t.co/3mLxc4mvsZ", text: ["top", "5", "smartphones", "india", "yes", "xiaomi", "redmi", "5a", "surprise", "names", "might", "indian", "express"], label: "pos"}, {raw_text: "Here are the top 5 smartphones in India: Yes, Xiaomi Redmi 5A is no surprise, but the other names might be - The Indian Express https://t.co/lON8EkLq5x", text: ["top", "5", "smartphones", "india", "yes", "xiaomi", "redmi", "5a", "surprise", "names", "might", "indian", "express"], label: "pos"}, {raw_text: "Here are the top 5 smartphones in India: Yes, Xiaomi Redmi 5A is no surprise, but the other names might be - The Indian Express https://t.co/7ABnkjT3PT", text: ["top", "5", "smartphones", "india", "yes", "xiaomi", "redmi", "5a", "surprise", "names", "might", "indian", "express"], label: "pos"}, {raw_text: "Here are the top 5 smartphones in India: Yes, Xiaomi Redmi 5A is no surprise, but the other names might be - The Indian Express https://t.co/q3NlWOz07Z https://t.co/DeWnmaDb6o", text: ["top", "5", "smartphones", "india", "yes", "xiaomi", "redmi", "5a", "surprise", "names", "might", "indian", "express"], label: "pos"}, {raw_text: "Here are the top 5 smartphones in India: Yes, Xiaomi Redmi 5A is no surprise, but the other names might be https://t.co/hEVqI1t6vV https://t.co/KH1gpnbEUZ", text: ["top", "5", "smartphones", "india", "yes", "xiaomi", "redmi", "5a", "surprise", "names", "might"], label: "pos"}, {raw_text: "Here are the top 5 smartphones in India: Yes, Xiaomi Redmi 5A is no surprise, but the other names might b.. https://t.co/xeOM3muiRp", text: ["top", "5", "smartphones", "india", "yes", "xiaomi", "redmi", "5a", "surprise", "names", "might", "b"], label: "pos"}], positive: 0.64, negative: 0.36, tweets_count: 11, keyword: "\"\"xiaomi redmi 5a\"\"", queries_done: 1}
